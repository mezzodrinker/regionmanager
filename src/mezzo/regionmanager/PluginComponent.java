package mezzo.regionmanager;

/**
 * 
 * @author mezzodrinker
 * @since
 */
public abstract class PluginComponent {
    public void onLoad() {}

    public void onEnable() {}

    public void onDisable() {}
}
