package mezzo.regionmanager;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import mezzo.bukkit.BukkitPlugin;
import mezzo.regionmanager.command.CommandHandler;
import mezzo.util.logging.Level;
import mezzo.util.logging.file.DefaultFileLogger;
import mezzo.util.logging.file.FileLogger;
import mezzo.util.version.Version;

/**
 * 
 * @author mezzodrinker
 * @since
 */
public class Plugin extends BukkitPlugin {
    private Set<PluginComponent>  components    = new HashSet<PluginComponent>();

    private static CommandHandler commandHandler;
    private static InputCompiler  inputCompiler;
    private static FileLogger     logger;

    public static final int       cursePluginID = 54103;

    public Plugin() {
        super(cursePluginID);
    }

    @Override
    public void onLoad() {
        // initialize the internal logger
        getLogger().info("loading logger...");
        try {
            logger = new DefaultFileLogger("regionmanager.log") {
                @Override
                public void log(Object o, Level level) {
                    super.log(o, level);
                    getLogger().log(level.getJavaUtilEquiv(), String.valueOf(o));
                }
            };
        } catch (Throwable t) {
            // disable the plugin if the logger failed to initialize
            getLogger().log(java.util.logging.Level.SEVERE, "could not initialize logger", t);
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        logger.info("done.");

        // initialize the input compiler
        logger.info("loading input compiler...");
        try {
            inputCompiler = new InputCompiler(this);
            inputCompiler.onLoad();
            components.add(inputCompiler);
        } catch (Throwable t) {
            // disable the plugin if the input compiler failed to initialize
            logger.log("could not initialize input compiler", t);
            getServer().getPluginManager().disablePlugin(this);
            return;
        }

        // initialize the command handler
        try {
            commandHandler = new CommandHandler(this);
            commandHandler.onLoad();
            components.add(commandHandler);
        } catch (Throwable t) {
            // disable the plugin if the command handler failed to initialize
            logger.log("could not initialize command handler", t);
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
    }

    @Override
    public void onEnable() {

    }

    @Override
    public void onDisable() {

    }

    @Override
    public boolean onCommand(org.bukkit.command.CommandSender sender, org.bukkit.command.Command command, String label, String[] args) {
        return false;
    }

    @Override
    public Version getVersion() {
        return null;
    }

    public static Locale getLocale() {
        // TODO Plugin.getLocale()
        return Locale.getDefault();
    }
}
