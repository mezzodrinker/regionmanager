package mezzo.regionmanager.command.regex;

/**
 * 
 * @author mezzodrinker
 * @since
 */
@SuppressWarnings("serial")
public class SyntaxException extends CommandRegexException {
    public SyntaxException() {
        super();
    }

    public SyntaxException(String message) {
        super(message);
    }

    public SyntaxException(Throwable cause) {
        super(cause);
    }

    public SyntaxException(String message, Throwable cause) {
        super(message, cause);
    }
}
