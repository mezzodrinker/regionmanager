package mezzo.regionmanager.command.regex;

import java.lang.reflect.Field;
import java.util.Objects;

/**
 * <code>Quantifier</code>
 * 
 * @author mezzodrinker
 * @since
 */
public class Quantifier {
    /**
     * <tt>true</tt> if this quantifier will be executed as long as there's possible matches available or {@link #maximumMatches} is reached (greedy behaviour).
     * 
     * <p>
     * <tt>false</tt> if this quantifier will executed as long as {@link #minimumMatches} hasn't been reached yet or there's possible matches available that don't match the following part of the
     * regular expression (lazy behavior).
     * </p>
     */
    public final boolean           greedy;

    /**
     * Minimum amount of matches this quantifier allows.
     */
    public final int               minimumMatches;

    /**
     * Maximum amount of matches this quantifier allows.
     */
    public final int               maximumMatches;

    /**
     * String representation of the quantifier that will be used throughout any regular expressions
     */
    public final String            name;

    /**
     * Allows at least zero matches. <strong>{@linkplain #greedy Greedy}</strong>.
     */
    public static final Quantifier ZERO_PLUS_GREEDY = new Quantifier("0...", 0, -1, true);

    /**
     * Allows at least zero matches. <strong>{@linkplain #greedy Lazy}</strong>.
     */
    public static final Quantifier ZERO_PLUS_LAZY   = new Quantifier("0...", 0, -1, false);

    /**
     * Allows exactly one match.
     */
    public static final Quantifier ONE              = new Quantifier("1", 1, 1, true);

    /**
     * Allows at least one match. <strong>{@linkplain #greedy Greedy}</strong>.
     */
    public static final Quantifier ONE_PLUS_GREEDY  = new Quantifier("1...", 1, -1, true);

    /**
     * Allows at least one match. <strong>{@linkplain #greedy Lazy}</strong>.
     */
    public static final Quantifier ONE_PLUS_LAZY    = new Quantifier("1...?", 1, -1, false);

    /**
     * Allows at most one match. <strong>{@linkplain #greedy Greedy}</strong>.
     */
    public static final Quantifier ONE_LESS_GREEDY  = new Quantifier("...1", 0, 1, true);

    /**
     * Allows at most one match. <strong>{@linkplain #greedy Lazy}</strong>.
     */
    public static final Quantifier ONE_LESS_LAZY    = new Quantifier("...?1", 0, 1, false);

    /**
     * Creates a new quantifier.
     * 
     * @param name
     *            string representation of the quantifier that will be used throughout any regular expressions
     * @param minimumMatches
     *            see {@link #minimumMatches}
     * @param maximumMatches
     *            see {@link #maximumMatches}
     * @param greedy
     *            see {@link #greedy}
     */
    public Quantifier(String name, int minimumMatches, int maximumMatches, boolean greedy) {
        Objects.requireNonNull(name);
        this.name = name;
        this.minimumMatches = minimumMatches;
        this.maximumMatches = maximumMatches;
        this.greedy = greedy;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(getClass().getName());
        s.append('@').append(Integer.toHexString(hashCode()));
        s.append('{');
        s.append("name=").append(name);
        s.append(",min=").append(minimumMatches);
        s.append(",max=").append(maximumMatches);
        s.append(",greedy=").append(greedy);
        s.append('}');
        return s.toString();
    }

    /**
     * Receives a predefined quantifier or creates a new one based on input from a string.
     * 
     * @param input
     *            input string to use for identifying or creating the correct quantifier
     * @return quantifier that is represented by the input string
     */
    public static Quantifier get(String input) {
        Objects.requireNonNull(input);
        if (input.length() <= 0) throw new SyntaxException("no such quantifier: " + input);

        // check for predefined quantifiers
        for (Field f : Quantifier.class.getDeclaredFields()) {
            try {
                Quantifier q = (Quantifier) f.get(null);
                if (q.name.equals(input)) return q;
            } catch (Throwable t) {
                continue;
            }
        }

        // create a new quantifier
        try {
            if (input.startsWith("...?")) {
                int max = Integer.valueOf(input.replace("...?", ""));
                if (max < 0) throw new IllegalArgumentException("maximum boundary must be >= 0, is " + max);
                return new Quantifier(input, 0, max, false);
            }

            if (input.startsWith("...")) {
                int max = Integer.valueOf(input.replace("...", ""));
                if (max < 0) throw new IllegalArgumentException("maximum boundary must be >= 0, is " + max);
                return new Quantifier(input, 0, max, true);
            }

            if (input.endsWith("...?")) {
                int min = Integer.valueOf(input.replace("...?", ""));
                if (min < 0) throw new IllegalArgumentException("minimum boundary must be >= 0, is " + min);
                return new Quantifier(input, min, -1, false);
            }

            if (input.endsWith("...")) {
                int min = Integer.valueOf(input.replace("...", ""));
                if (min < 0) throw new IllegalArgumentException("minimum boundary must be >= 0, is " + min);
                return new Quantifier(input, min, -1, true);
            }

            if (input.contains("...?")) {
                String[] array = input.split("\\.\\.\\.\\?");
                int min = Integer.valueOf(array[0]);
                int max = Integer.valueOf(array[1]);
                if (min < 0) throw new IllegalArgumentException("minimum boundary must be >= 0, is " + min);
                if (max < min) throw new IllegalArgumentException("maximum boundary must be >= " + min + ", is " + max);
                return new Quantifier(input, min, max, false);
            }

            if (input.contains("...")) {
                String[] array = input.split("\\.\\.\\.");
                int min = Integer.valueOf(array[0]);
                int max = Integer.valueOf(array[1]);
                if (min < 0) throw new IllegalArgumentException("minimum boundary must be >= 0, is " + min);
                if (max < min) throw new IllegalArgumentException("maximum boundary must be >= " + min + ", is " + max);
                return new Quantifier(input, min, max, true);
            }

            int i = Integer.valueOf(input);
            if (i < 0) throw new IllegalArgumentException("boundary must be >= 0");
            return new Quantifier(input, i, i, true);
        } catch (Throwable t) {
            throw new CommandRegexException("caught exception while trying to create new quantifier " + input, t);
        }
    }
}
