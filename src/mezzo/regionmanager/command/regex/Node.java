package mezzo.regionmanager.command.regex;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import mezzo.regionmanager.command.CommandPart;
import mezzo.regionmanager.command.CommandPart.Type;
import mezzo.util.ArrayUtil;
import mezzo.util.CharSequenceUtil;
import mezzo.util.ReturnValue2;

/**
 * 
 * @author mezzodrinker
 * @since
 */
class Node {
    private String            value      = "";
    private String            identifier = "";
    private Type              type       = Type.STRING;
    private Quantifier        quantifier = Quantifier.ONE;
    private List<Node>        children   = new ArrayList<>();

    List<CommandPart>         matches    = new ArrayList<>();

    public final CharSequence pattern;
    public final Node         parent;
    public final CommandRegex regex;

    protected Node(CharSequence pattern, Node parent, CommandRegex regex) {
        this.pattern = pattern;
        this.parent = parent;
        this.regex = Objects.requireNonNull(regex);
    }

    public Node(CharSequence pattern, CommandRegex regex) {
        this(pattern, null, regex);
    }

    /**
     * Finishes the compilation process by replacing all children in {@link #value} by <tt>[&lt;index&gt;]</tt>.
     */
    private void finish() {
        int count = 0;
        for (; count < children.size(); count++) {
            value = value.replaceFirst(Pattern.quote(children.get(count).pattern.toString()), "[" + count + "]");
        }
    }

    private Node next(Node child) {
        int nextPos = children.indexOf(child) + 1;
        if (nextPos == 0 || nextPos >= children.size()) return null;
        return children.get(nextPos);
    }

    protected ReturnValue2<Integer, Integer> matches(CommandPart[] array, int start) {
        if (isRoot()) throw new UnsupportedOperationException("matches(CharSequence[], int) is not supported for root nodes");

        // if the end of the message has been reached, return -1 if the minimum allowed match count of the quantifier is greater than 0, otherwise return the message length
        if (start >= array.length) {
            if (quantifier.minimumMatches > 0) return new ReturnValue2<>(-1, 0);
            return new ReturnValue2<>(array.length, 0);
        }

        if (!hasChildren()) {
            int matchCount = 0;

            Pattern regex = null;
            if (value.length() > 2 && value.charAt(0) == '#' && value.charAt(value.length() - 2) != '\\' && value.charAt(value.length() - 1) == '#') {
                regex = Pattern.compile(value.substring(1, value.length() - 1));
            }

            for (; matchCount < quantifier.maximumMatches && start + matchCount < array.length; matchCount++) {
                if (parent != null && matchCount >= quantifier.minimumMatches && !quantifier.greedy) {
                    Node next = parent.next(this);
                    if (next != null) {
                        ReturnValue2<Integer, Integer> match = next.matches(array, Math.min(start + matchCount, array.length));
                        if (match.value1 > 0) return new ReturnValue2<>(match.value1, match.value2 + 1);
                    }
                }

                CommandPart current = array[start + matchCount];

                // this node matches the current part, if
                // - value is empty and type is either STRING or matches the current part's type
                // - value equals the current part and type is either STRING or matches the current part's type
                boolean matches = (regex == null ? (value.length() <= 0 || CharSequenceUtil.equals(value, current)) : regex.matcher(current).matches()) && current.is(type);

                if (!matches) {
                    break;
                }

                this.matches.add(current);
            }

            // if the match count is less than the minimum allowed match count of the quantifier, return -1 to indicate this node does not match the current message part
            if (matchCount < quantifier.minimumMatches) return new ReturnValue2<>(-1, 0);
            return new ReturnValue2<>(Math.min(start + matchCount, array.length), 0);
        }

        // if this node has children, ignore everything else except these children and the quantifier
        int matchCount = 0;
        for (; matchCount < quantifier.maximumMatches && start + matchCount < array.length; matchCount++) {
            for (Node child : children) {
                ReturnValue2<Integer, Integer> match = child.matches(array, start);

                // if the returned new position is less than 0, return -1 to indicate this node does not match the current message section
                if (match.value1 < 0) return new ReturnValue2<>(-1, 0);
                start = match.value1;
            }
        }

        // if the match count is less than the minimum allowed match count of the quantifier, return -1 to indicate this node does not match the current message section
        if (matchCount < quantifier.minimumMatches) return new ReturnValue2<>(-1, 0);
        return new ReturnValue2<>(Math.min(start, array.length), 0);
    }

    /**
     * @return <tt>true</tt> if this node is a root node (has no parent node), <tt>false</tt> if not.
     */
    public boolean isRoot() {
        return parent == null;
    }

    /**
     * @return <tt>true</tt> if this node has at least one child, <tt>false</tt> if not.
     */
    public boolean hasChildren() {
        return !children.isEmpty();
    }

    public void compile() {
        // only the root node can check if the pattern is valid or not
        if (isRoot()) {
            CommandRegex.check(pattern);
        }

        // start index of the actual code (excludes [ at the beginning)
        final int start = pattern.charAt(0) == '[' ? 1 : 0;
        // end index of the actual code (excludes ] at the end)
        final int end = pattern.charAt(pattern.length() - 1) == ']' ? pattern.length() - 1 : pattern.length();

        // use iteration instead of recursion to prevent StackOverflowExceptions
        boolean escaped = false;
        for (int i = start; i < end; i++) {
            char current = pattern.charAt(i);

            if (!escaped) {
                if (current == '\\') {
                    escaped = true;
                    continue;
                }
                if (current == '[') {
                    CharSequence block = CommandRegex.block(pattern, i);

                    // don't add children for empty blocks
                    if (block.length() > 0) {
                        Node child = new Node(block, this, regex);
                        child.compile();
                        children.add(child);

                        // increase the iterator count by the block's length to prevent accidental duplication
                        // -1 so that [...][...] is also possible (because of i++ in for loop)
                        i += block.length() - 1;
                    }
                }
            }
            escaped = false;
        }

        // the actual code, excluding surrounding [] of this node
        CharSequence code = pattern.subSequence(start, end);

        // node syntax: '[' ([value] [':' <type>] | <nested regex>) ['|' <quantifier>] ['~' <identifier>]']'
        // index 0 - <value> or <nested regex>
        // index 1 - <type>, if any
        // index 2 - <quantifier>, if any
        // index 3 - <identifier>, if any
        String[] array = new String[4];
        {
            StringBuilder[] builders = ArrayUtil.create(StringBuilder.class, 4);
            int c = 0;
            escaped = false;
            int scope = 0;
            for (int i = 0; i < code.length(); i++) {
                char current = code.charAt(i);
                if (!escaped) {
                    if (current == '\\') {
                        builders[c].append(current);
                        escaped = true;
                        continue;
                    }
                    if (current == '[') {
                        scope++;
                    } else if (current == ']') {
                        scope--;
                    } else {
                        if (current == ':' && scope == 0) {
                            c = 1;
                            continue;
                        }
                        if (current == '|' && scope == 0) {
                            c = 2;
                            continue;
                        }
                        if (current == '~' && scope == 0) {
                            c = 3;
                            continue;
                        }
                    }
                }
                builders[c].append(current);
                escaped = false;
            }
            array = ArrayUtil.convert(builders, element -> element.toString(), String.class);
        }

        value = array[0];
        if (array[1].length() > 0) {
            type = Type.forName(array[1]);
        }
        if (array[2].length() > 0) {
            quantifier = Quantifier.get(array[2]);
        }
        if (array[3].length() > 0) {
            identifier = array[3];
            regex.identifiers.put(identifier, this);
        }

        // finish compiling
        finish();
    }

    public boolean matches(CommandPart[] array) {
        if (!isRoot()) throw new UnsupportedOperationException("matches(CommandPart[]) is not supported for non-root nodes");

        int currentPos = 0;
        int toSkip = 0;
        for (Node child : children) {
            if (toSkip > 0) {
                toSkip--;
                continue;
            }
            ReturnValue2<Integer, Integer> ret = child.matches(array, currentPos);
            currentPos = ret.value1;
            toSkip = ret.value2;
            if (currentPos < 0) return false;
        }

        return currentPos >= array.length;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(getClass().getName()); // class name
        s.append('@').append(Integer.toHexString(hashCode())); // hash code
        s.append('{');
        s.append("value=").append(value);
        s.append(",identifier=").append(identifier);
        s.append(",type=").append(type);
        s.append(",quantifier=").append(quantifier);
        s.append(",children=").append(ArrayUtil.toString(children.toArray(new Node[0])));
        s.append(",matches=").append(ArrayUtil.toString(matches.toArray(new CommandPart[0])));
        s.append('}');
        return s.toString();
    }
}
