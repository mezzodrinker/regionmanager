package mezzo.regionmanager.command.regex;

/**
 * 
 * @author mezzodrinker
 * @since
 */
@SuppressWarnings("serial")
public class CommandRegexException extends RuntimeException {
    public CommandRegexException() {
        super();
    }

    public CommandRegexException(String message) {
        super(message);
    }

    public CommandRegexException(Throwable cause) {
        super(cause);
    }

    public CommandRegexException(String message, Throwable cause) {
        super(message, cause);
    }
}
