package mezzo.regionmanager.command.regex;

import java.util.HashMap;
import java.util.Map;

import mezzo.regionmanager.command.Command;
import mezzo.regionmanager.command.CommandPart;
import mezzo.util.CharSequenceUtil.DefaultCharSequence;

/**
 * 
 * @author mezzodrinker
 * @since
 */
public class CommandRegex {
    private Node      root        = null;

    Map<String, Node> identifiers = new HashMap<String, Node>();

    private CommandRegex(CharSequence regex) {
        root = new Node(regex, this);
        root.compile();
    }

    protected static void check(CharSequence regex) {
        int opened = 0;
        int closed = 0;
        boolean escaped = false;
        for (int i = 0; i < regex.length(); i++) {
            char current = regex.charAt(i);
            if (!escaped) {
                if (current == '\\') {
                    escaped = true;
                    continue;
                }
                if (current == '[') {
                    opened++;
                    continue;
                }
                if (current == ']') {
                    closed++;
                    if (closed > opened) throw new SyntaxException("unopened bracket at index " + i);
                    continue;
                }
            }
            escaped = false;
        }
        if (opened > closed) throw new SyntaxException((opened - closed) + " unclosed brackets");
    }

    protected static CharSequence block(CharSequence regex, int index) {
        StringBuilder block = new StringBuilder();
        int open = 0;
        boolean escaped = false;
        for (int i = index; i < regex.length(); i++) {
            char current = regex.charAt(i);
            if (!escaped) {
                if (current == '\\') {
                    escaped = true;
                    continue;
                }
                if (current == '[') {
                    open++;
                }
                if (current == ']') {
                    open--;
                }
            }
            block.append(current);
            escaped = false;
            if (open <= 0) {
                break;
            }
        }
        return new DefaultCharSequence(block);
    }

    public boolean matches(CommandPart[] input) {
        return root.matches(input);
    }

    public boolean matches(Command input) {
        return root.matches(input.command);
    }

    public CommandPart[] getMatches(String identifier) {
        Node node = identifiers.get(identifier);
        if (node == null) throw new IllegalArgumentException("identifier '" + identifier + "' is not registered");
        return node.matches.toArray(new CommandPart[0]);
    }

    public CharSequence pattern() {
        return root.pattern;
    }

    public static CommandRegex compile(CharSequence regex) {
        return new CommandRegex(regex);
    }
}
