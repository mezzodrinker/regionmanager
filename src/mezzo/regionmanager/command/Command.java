package mezzo.regionmanager.command;

import mezzo.util.ArrayUtil;

/**
 * 
 * @author mezzodrinker
 * @since
 */
public class Command {
    public final CommandPart[] command;

    public Command(CharSequence raw) {
        this(parse(raw));
    }

    public Command(String[] raw) {
        this(parse(raw));
    }

    public Command(CommandPart[] command) {
        this.command = command;
    }

    public Command(Command command) {
        this.command = command.command;
    }

    private static String[] split(CharSequence raw) {
        // create an array with the same length of raw that contains empty string builders only
        StringBuilder[] array = ArrayUtil.create(StringBuilder.class, raw.length());

        // split raw
        int c = 0;
        for (int i = 0; i < raw.length(); i++) {
            char current = raw.charAt(i);
            if (Character.isWhitespace(current)) {
                c++;
                continue;
            }
            array[c].append(current);
        }

        // eliminate null or empty elements in array and convert it to Strings
        String[] ret = ArrayUtil.remove(array, element -> element == null || element.length() <= 0, element -> element.toString(), String.class);
        return ret;
    }

    private static String[] quote(String[] raw) {
        // create an array with the same length of raw that contains empty string builders only
        StringBuilder[] array = ArrayUtil.create(StringBuilder.class, raw.length);

        // combine elements enclosed by quotation marks (")
        int c = 0;
        boolean quoted = false;
        for (String element : raw) {
            if (element.charAt(0) == '"') {
                quoted = !quoted;
                if (!quoted) {
                    c++;
                }
            }
            if (quoted) {
                array[c].append(array[c].length() > 0 ? " " : "").append(element.replace("\"", ""));
            } else {
                array[c++].append(element);
            }
            if (element.charAt(element.length() - 1) == '"' && element.charAt(element.length() - 2) != '\\') {
                quoted = !quoted;
                if (!quoted) {
                    c++;
                }
            }
        }

        // eliminate null or empty elements in array and convert it to Strings
        return ArrayUtil.remove(array, element -> element == null || element.length() <= 0, element -> element.toString(), String.class);
    }

    private static CommandPart[] interpret(String[] raw) {
        CommandPart[] ret = ArrayUtil.convert(raw, element -> new CommandPart(element), CommandPart.class);
        return ret;
    }

    private static CommandPart[] parse(String[] raw) {
        CommandPart[] array = null;

        // combine texts enclosed by quotation marks (")
        raw = quote(raw);

        // do an interpretation of the input
        array = interpret(raw);

        return array;
    }

    private static CommandPart[] parse(CharSequence raw) {
        String[] array = null;

        // split the string
        array = split(raw);

        return parse(array);
    }
}
