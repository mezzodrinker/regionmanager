package mezzo.regionmanager.command;

import mezzo.regionmanager.Plugin;
import mezzo.util.CharSequenceUtil;
import mezzo.util.CharSequenceUtil.DefaultCharSequence;
import mezzo.util.text.BooleanParser;
import mezzo.util.text.FloatParser;
import mezzo.util.text.IntegerParser;
import mezzo.util.text.StringArrayParser;
import mezzo.util.text.StringParser;

/**
 * 
 * @author mezzodrinker
 * @since
 */
public class CommandPart extends DefaultCharSequence {
    public final Type                     type;
    public static final IntegerParser     INTEGER_PARSER      = IntegerParser.getInstance();
    public static final FloatParser       FLOAT_PARSER        = FloatParser.getInstance();
    public static final BooleanParser     BOOLEAN_PARSER      = BooleanParser.getInstance(Plugin.getLocale());
    public static final StringArrayParser STRING_ARRAY_PARSER = StringArrayParser.getInstance();
    public static final StringParser      STRING_PARSER       = StringParser.getInstance();

    public CommandPart(char[] content) {
        super(content);
        type = type();
    }

    public CommandPart(CharSequence content) {
        super(content);
        type = type();
    }

    private Type type() {
        if (INTEGER_PARSER.test(this)) return Type.INTEGER;
        if (FLOAT_PARSER.test(this)) return Type.FLOAT;
        if (BOOLEAN_PARSER.test(this)) return Type.BOOLEAN;
        if (STRING_ARRAY_PARSER.test(this)) return Type.STRING_ARRAY;
        return Type.STRING;
    }

    public boolean has(Type type) {
        return this.type == type;
    }

    public boolean is(Type type) {
        switch (type) {
            case INTEGER:
                return isInteger();
            case FLOAT:
                return isFloat();
            case BOOLEAN:
                return isBoolean();
            case STRING_ARRAY:
                return isStringArray();
            case STRING:
                return true;
        }
        return false;
    }

    public boolean isInteger() {
        return type == Type.INTEGER;
    }

    public boolean isFloat() {
        return type == Type.INTEGER || type == Type.FLOAT;
    }

    public boolean isBoolean() {
        return type == Type.BOOLEAN;
    }

    public boolean isStringArray() {
        return type == Type.STRING_ARRAY;
    }

    public int toInteger() {
        return INTEGER_PARSER.parse(this);
    }

    public float toFloat() {
        return FLOAT_PARSER.parse(this);
    }

    public boolean toBoolean() {
        return BOOLEAN_PARSER.parse(this);
    }

    public String[] toStringArray() {
        return STRING_ARRAY_PARSER.parse(this);
    }

    public static enum Type {
        INTEGER("int", "integer"), FLOAT("float"), BOOLEAN("bool", "boolean"), STRING("string"), STRING_ARRAY("string array", "string[]");

        private final String[] strings;

        private Type(String... strings) {
            this.strings = strings;
        }

        public static Type forName(CharSequence name) {
            for (Type type : Type.values()) {
                for (String s : type.strings) {
                    if (CharSequenceUtil.equalsIgnoreCase(s, name)) return type;
                }
            }
            return null;
        }
    }
}