package mezzo.regionmanager.command;

import java.util.Arrays;

/**
 * 
 * @author mezzodrinker
 * @since
 */
public class Keyword {
    private int       count   = 0;
    private Command[] aliases = null;

    public Keyword(CharSequence... aliases) {
        if (aliases == null) throw new NullPointerException("a keyword requires at least one alias");
        if (aliases.length <= 0) throw new IllegalArgumentException("a keyword requires at least one alias");
        this.aliases = new Command[aliases.length];
        for (int i = 0; i < aliases.length; i++) {
            this.aliases[i] = new Command(aliases[i]);
        }
        count = aliases.length;
    }

    public Keyword(Command... aliases) {
        if (aliases == null) throw new NullPointerException("a keyword requires at least one alias");
        if (aliases.length <= 0) throw new IllegalArgumentException("a keyword requires at least one alias");
        this.aliases = aliases;
        count = aliases.length;
    }

    public void addAlias(Command alias) {
        if (count >= aliases.length) {
            aliases = Arrays.copyOf(aliases, count + 1);
        }
        aliases[count++] = alias;
    }

    public boolean removeAlias(Command alias) {
        int hits = 0;
        for (int i = 0; i < aliases.length; i++) {
            if (aliases[i].equals(alias)) {
                hits++;
                continue;
            }
            aliases[i - hits] = aliases[i];
        }
        if (hits > 0) {
            aliases = Arrays.copyOf(aliases, aliases.length - hits);
        }
        return hits > 0;
    }

    public Command[] getAliases() {
        return aliases;
    }
}
