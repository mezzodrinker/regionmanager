package mezzo.regionmanager.command;

import mezzo.regionmanager.Plugin;
import mezzo.regionmanager.PluginComponent;

/**
 * 
 * @author mezzodrinker
 * @since
 */
public class CommandHandler extends PluginComponent {
    public final Plugin plugin;

    public CommandHandler(Plugin plugin) {
        this.plugin = plugin;
    }
}
