package mezzo.zz_bukkit.regionmanager;

/**
 * <code>RegionManagerException</code>
 * 
 * @author mezzodrinker
 * @since
 */
@SuppressWarnings("serial")
public abstract class RegionManagerException extends Exception {
    @Override
    public abstract String getMessage();

    @Override
    public abstract String getLocalizedMessage();
}
