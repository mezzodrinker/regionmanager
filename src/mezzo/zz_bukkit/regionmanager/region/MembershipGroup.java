package mezzo.zz_bukkit.regionmanager.region;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

import mezzo.bukkit.Player;

/**
 * <code>MembershipGroup</code>
 * 
 * @author mezzodrinker
 * @since
 */
public class MembershipGroup {
    private Set<String> players = new HashSet<>();

    // TODO private Set<Group> groups = new HashSet<>();

    public boolean add(Player player) {
        return players.add(player.getName());
    }

    public boolean remove(Player player) {
        return players.remove(player.getName());
    }

    public boolean contains(Player player) {
        return players.contains(player.getName());
    }

    public void forEachPlayer(Consumer<? super String> action) {
        players.forEach(action);
    }
}
