package mezzo.zz_bukkit.regionmanager.region.exception;

import mezzo.zz_bukkit.regionmanager.RegionManagerException;
import mezzo.zz_bukkit.regionmanager.region.Region;

/**
 * <code>RegionException</code>
 * 
 * @author mezzodrinker
 */
@SuppressWarnings("serial")
public abstract class RegionException extends RegionManagerException {
    protected final Region region;

    protected RegionException(Region region) {
        if (region == null) throw new IllegalArgumentException("region can not be null");
        this.region = region;
    }

    public Region getRegion() {
        return region;
    }

    @Override
    public String toString() {
        return getClass() + "@" + Integer.toString(hashCode(), 16) + "{\"" + getMessage() + "\"}";
    }
}
