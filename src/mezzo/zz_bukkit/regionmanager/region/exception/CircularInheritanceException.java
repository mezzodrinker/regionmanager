package mezzo.zz_bukkit.regionmanager.region.exception;

import mezzo.zz_bukkit.regionmanager.region.Region;

/**
 * <code>CircularInheritanceException</code>
 * 
 * @author mezzodrinker
 * @since
 */
@SuppressWarnings("serial")
public class CircularInheritanceException extends RegionException {
    private Region parent = null;

    public CircularInheritanceException(Region region, Region parent) {
        super(region);
        this.parent = parent;
    }

    @Override
    public String getMessage() {
        return "setting the parent of " + region + " to " + parent + " would cause a circular inheritance";
    }

    @Override
    public String getLocalizedMessage() {
        return getMessage();
    }
}
