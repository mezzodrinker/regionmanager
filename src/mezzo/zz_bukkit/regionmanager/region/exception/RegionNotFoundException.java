package mezzo.zz_bukkit.regionmanager.region.exception;

import org.bukkit.World;

/**
 * <code>RegionNotFoundException</code>
 * 
 * @author mezzodrinker
 */
@SuppressWarnings("serial")
public class RegionNotFoundException extends Exception {
    protected final String regionname;
    protected final World  world;

    public RegionNotFoundException(String regionname, World world) {
        this.world = world;
        this.regionname = regionname;
    }

    public World getWorld() {
        return world;
    }

    public String getRegionname() {
        return regionname;
    }
}
