package mezzo.zz_bukkit.regionmanager.region.exception;

import mezzo.zz_bukkit.regionmanager.region.Region;

/**
 * <code>RegionExistsException</code>
 * 
 * @author mezzodrinker
 * @since
 */
@SuppressWarnings("serial")
public class RegionExistsException extends RegionException {
    public RegionExistsException(Region region) {
        super(region);
    }

    @Override
    public String getMessage() {
        return region + " does already exist";
    }

    @Override
    public String getLocalizedMessage() {
        return getMessage();
    }
}
