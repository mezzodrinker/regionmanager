package mezzo.zz_bukkit.regionmanager.region;

import java.util.List;
import java.util.Set;

import mezzo.bukkit.BlockVector2;
import mezzo.bukkit.BlockVector3;
import mezzo.util.math.Vector;

/**
 * <code>CubicRegion</code>
 * 
 * @author mezzodrinker
 */
public class CubicRegion extends Region {
    public CubicRegion(String id, List<? extends BlockVector3> points) {
        super(id);
        setPoints(points);
    }

    @Override
    public Set<BlockVector2> getPoints() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean contains(Vector v) {
        // TODO Auto-generated method stub
        return false;
    }
}
