package mezzo.zz_bukkit.regionmanager.region;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.function.Function;

import mezzo.zz_bukkit.regionmanager.region.exception.RegionExistsException;
import mezzo.zz_bukkit.regionmanager.region.exception.RegionNotFoundException;

import org.bukkit.World;

/**
 * <code>RegionHandler</code>
 * 
 * @author mezzodrinker
 */
public abstract class RegionHandler {
    protected Region      __global__;
    protected final World world;

    protected RegionHandler(World world) {
        this.world = world;
    }

    public Region getGlobalRegion() {
        return __global__;
    }

    public World getWorld() {
        return world;
    }

    public abstract void save() throws IOException;

    public abstract void load() throws IOException;

    public abstract void add(Region region) throws RegionExistsException;

    public abstract void add(String key) throws RegionExistsException;

    public abstract void remove(Region region) throws RegionNotFoundException;

    public abstract void remove(String key) throws RegionNotFoundException;

    public abstract boolean exists(String key);

    public abstract Region getRegion(String key);

    public abstract Region getRegionExact(String key);

    public abstract Map<String, Region> getRegions();

    public abstract Collection<Region> getRegions(Function<Region, Boolean> where);
}
