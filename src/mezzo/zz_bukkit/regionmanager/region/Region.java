package mezzo.zz_bukkit.regionmanager.region;

import java.util.List;
import java.util.Set;

import mezzo.bukkit.BlockVector2;
import mezzo.bukkit.BlockVector3;
import mezzo.bukkit.Player;
import mezzo.util.math.Vector;
import mezzo.util.math.Vector3;
import mezzo.zz_bukkit.regionmanager.region.exception.CircularInheritanceException;

/**
 * <code>Region</code>
 * 
 * @author mezzodrinker
 */
public abstract class Region implements Comparable<Region> {
    protected int             priority = 0;
    protected String          id       = null;
    protected Region          parent   = null;
    protected BlockVector3    min      = null;
    protected BlockVector3    max      = null;
    protected MembershipGroup members  = new MembershipGroup();
    protected MembershipGroup owners   = new MembershipGroup();

    protected Region(String id) {
        this.id = id;
    }

    protected void setPoints(List<? extends BlockVector3> points) {
        BlockVector3 first = points.get(0);
        int minX = first.blockX;
        int minY = first.blockY;
        int minZ = first.blockZ;
        int maxX = minX;
        int maxY = minY;
        int maxZ = minZ;

        for (BlockVector3 v : points) {
            if (v.blockX < minX) {
                minX = v.blockX;
            }
            if (v.blockY < minY) {
                minY = v.blockY;
            }
            if (v.blockZ < minZ) {
                minZ = v.blockZ;
            }
            if (v.blockX > maxX) {
                maxX = v.blockX;
            }
            if (v.blockY > maxY) {
                maxY = v.blockY;
            }
            if (v.blockZ > maxZ) {
                maxZ = v.blockZ;
            }
        }

        min = new BlockVector3(minX, minY, minZ);
        max = new BlockVector3(maxX, maxY, maxZ);
    }

    public String getID() {
        return id;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }

    public BlockVector3 getMinimumPoint() {
        return min;
    }

    public BlockVector3 getMaximumPoint() {
        return max;
    }

    public MembershipGroup getMembers() {
        return members;
    }

    public void setMembers(MembershipGroup members) {
        this.members = members;
    }

    public boolean isMember(Player player) {
        Region r = this;
        do {
            if (r.getOwners().contains(player)) return true;
        } while ((r = r.getParent()) != null);
        return false;
    }

    public MembershipGroup getOwners() {
        return owners;
    }

    public void setOwners(MembershipGroup owners) {
        this.owners = owners;
    }

    public boolean isOwner(Player player) {
        Region r = this;
        do {
            if (r.getOwners().contains(player)) return true;
        } while ((r = r.getParent()) != null);
        return false;
    }

    public Region getParent() {
        return parent;
    }

    public void setParent(Region region) throws CircularInheritanceException {
        if (region == null) {
            parent = region;
            return;
        }
        Region r = region;
        do {
            if (r == this) throw new CircularInheritanceException(this, r);
        } while ((r = r.getParent()) != null);
        parent = region;
    }

    public boolean contains(BlockVector2 v) {
        return contains(new BlockVector3(v.blockX, min.blockY, v.blockZ));
    }

    public boolean contains(double x, double y, double z) {
        return contains(new Vector3(x, y, z));
    }

    public boolean containsAny(Set<BlockVector2> points) {
        for (BlockVector2 v : points) {
            if (contains(v)) return true;
        }
        return false;
    }

    @Override
    public int compareTo(Region region) {
        int ret = priority - region.priority;
        if (ret == 0) return id.compareTo(region.getID());
        return ret;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Region && ((Region) o).getID().equals(getID());
    }

    public abstract Set<BlockVector2> getPoints();

    public abstract boolean contains(Vector v);
}
