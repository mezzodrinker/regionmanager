package mezzo.zz_bukkit.regionmanager.command;

import mezzo.zz_bukkit.regionmanager.permission.Permission;

import com.sun.xml.internal.ws.api.message.Message;

/**
 * <code>Command</code>
 * 
 * @author mezzodrinker
 */
public class Command {
    private SyntaxBuilder     syntaxBuilder = null;

    public final boolean      isAdminCommand;
    public final Command      superCommand;
    public final Message      description;
    public final Permission   permission;
    public final ArgumentList arguments;
    public final String[]     labels;

    public Command(String label, Command superCommand, Message description, Permission permission, ArgumentList arguments, boolean isAdminCommand) {
        this(new String[] {label}, superCommand, description, permission, arguments, isAdminCommand);
    }

    public Command(String[] labels, Command superCommand, Message description, Permission permission, ArgumentList arguments, boolean isAdminCommand) {
        this.labels = labels;
        this.superCommand = superCommand;
        this.description = description;
        this.permission = permission;
        this.arguments = arguments;
        this.isAdminCommand = isAdminCommand;
    }

    public boolean isSubCommand() {
        return superCommand != null;
    }

    public boolean isPlayerCommand() {
        return arguments.isPlayerCommand;
    }

    public boolean isServerCommand() {
        return arguments.isServerCommand;
    }

    public String[] concatenate(String[] args, boolean isPlayer) {
        return arguments.concatenate(args, isPlayer);
    }

    public SyntaxBuilder getSyntaxBuilder() {
        if (syntaxBuilder == null) {
            syntaxBuilder = new SyntaxBuilder(this);
        }
        return syntaxBuilder;
    }

    @Override
    public String toString() {
        return getClass() + "@" + Integer.toString(hashCode(), 16) + "{" + getSyntaxBuilder().getSyntax() + "}";
    }
}
