package mezzo.zz_bukkit.regionmanager.command;

import java.util.Arrays;

import mezzo.zz_bukkit.regionmanager.command.exception.CommandNotAvailableException;
import mezzo.zz_bukkit.regionmanager.command.exception.InvalidArgumentException;
import mezzo.zz_bukkit.regionmanager.command.exception.TooFewArgumentsException;
import mezzo.zz_bukkit.regionmanager.command.exception.TooManyArgumentsException;

import org.bukkit.command.CommandSender;

/**
 * <code>ArgumentList</code>
 * 
 * @author mezzodrinker
 * @since
 */
public class ArgumentList {
    public final boolean allowsConcatenation;
    public final boolean isPlayerCommand;
    public final boolean isServerCommand;
    /**
     * ascending ordered list of allowed argument counts for the server
     */
    public final int[]   allowedArgumentCounts_server;
    /**
     * ascending ordered list of allowed argument counts for players
     */
    public final int[]   allowedArgumentCounts_player;
    public final Command command;

    public ArgumentList(Command command) {
        allowsConcatenation = true;
        isPlayerCommand = true;
        isServerCommand = true;
        allowedArgumentCounts_server = null;
        allowedArgumentCounts_player = null;
        this.command = command;
    }

    public void checkArguments(CommandSender sender, String[] arguments) throws CommandNotAvailableException, TooManyArgumentsException, TooFewArgumentsException, InvalidArgumentException {
        boolean isPlayer = sender instanceof org.bukkit.entity.Player;
        if (isPlayer && !isPlayerCommand) throw new CommandNotAvailableException();
        if (!isPlayer && !isServerCommand) throw new CommandNotAvailableException();

        arguments = concatenate(arguments, sender instanceof org.bukkit.entity.Player);

        int max = 0;
        int min = Integer.MAX_VALUE;
        int[] allowedArgumentCounts = (isPlayer ? allowedArgumentCounts_player : allowedArgumentCounts_server);
        for (int argc : allowedArgumentCounts) {
            if (argc == arguments.length) return;
            if (argc > max) {
                max = argc;
            }
            if (argc < min) {
                min = argc;
            }
        }

        if (arguments.length > max) throw new TooManyArgumentsException(command, this, arguments);
        if (arguments.length < min) throw new TooFewArgumentsException(command, this, arguments);

        int index = 0;
        while (allowedArgumentCounts[index] < arguments.length) {
            index++;
        }
        throw new InvalidArgumentException(command, this, arguments, index);
    }

    public String[] concatenate(String[] arguments, boolean isPlayer) {
        int targetLength = 0;
        for (int argc : (isPlayer ? allowedArgumentCounts_player : allowedArgumentCounts_server)) {
            targetLength = argc;
            if (targetLength >= arguments.length) {
                break;
            }
        }
        if (!allowsConcatenation) return Arrays.copyOf(arguments, targetLength);

        String[] ret = new String[targetLength];
        for (int i = 0; i < arguments.length; i++) {
            if (i < targetLength - 1) {
                ret[i] = arguments[i];
                continue;
            }
            if (ret[targetLength - 1] == null) {
                ret[targetLength - 1] = arguments[i];
                continue;
            }
            ret[targetLength - 1] += " " + arguments[i];
        }
        return ret;
    }
}
