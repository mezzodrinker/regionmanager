package mezzo.zz_bukkit.regionmanager.command.exception;

/**
 * <code>CommandNotAvailableException</code>
 * 
 * @author mezzodrinker
 * @since
 */
@SuppressWarnings("serial")
public class CommandNotAvailableException extends CommandException {
    @Override
    public String getMessage() {
        return null;
    }

    @Override
    public String getLocalizedMessage() {
        return null;
    }
}
