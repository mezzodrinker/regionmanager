package mezzo.zz_bukkit.regionmanager.command.exception;

import mezzo.zz_bukkit.regionmanager.RegionManagerException;

/**
 * <code>CommandException</code>
 * 
 * @author mezzodrinker
 * @since
 */
@SuppressWarnings("serial")
public abstract class CommandException extends RegionManagerException {

}
