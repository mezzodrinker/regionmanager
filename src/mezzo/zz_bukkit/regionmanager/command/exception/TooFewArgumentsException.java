package mezzo.zz_bukkit.regionmanager.command.exception;

import mezzo.zz_bukkit.regionmanager.command.ArgumentList;
import mezzo.zz_bukkit.regionmanager.command.Command;

/**
 * <code>TooFewArgumentsException</code>
 * 
 * @author mezzodrinker
 * @since
 */
@SuppressWarnings("serial")
public class TooFewArgumentsException extends CommandException {
    private final Command      command;
    private final ArgumentList argumentList;
    private final String[]     args;

    public TooFewArgumentsException(Command command, ArgumentList argumentList, String[] arguments) {
        this.command = command;
        this.argumentList = argumentList;
        args = arguments;
    }

    public Command getCommand() {
        return command;
    }

    public ArgumentList getArgumentList() {
        return argumentList;
    }

    public String[] getArguments() {
        return args;
    }

    @Override
    public String getMessage() {
        return "too few arguments: " + command.getSyntaxBuilder().getSyntax() + " does not accept " + args.length + " arguments";
    }

    @Override
    public String getLocalizedMessage() {
        return getMessage();
    }
}
