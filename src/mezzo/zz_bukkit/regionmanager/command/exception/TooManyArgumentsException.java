package mezzo.zz_bukkit.regionmanager.command.exception;

import mezzo.zz_bukkit.regionmanager.command.ArgumentList;
import mezzo.zz_bukkit.regionmanager.command.Command;

/**
 * <code>TooManyArgumentsException</code>
 * 
 * @author mezzodrinker
 * @since
 */
@SuppressWarnings("serial")
public class TooManyArgumentsException extends CommandException {
    private final Command      command;
    private final ArgumentList argumentList;
    private final String[]     args;

    public TooManyArgumentsException(Command command, ArgumentList argumentList, String[] args) {
        this.command = command;
        this.argumentList = argumentList;
        this.args = args;
    }

    public Command getCommand() {
        return command;
    }

    public ArgumentList getArgumentList() {
        return argumentList;
    }

    public String[] getArguments() {
        return args;
    }

    @Override
    public String getMessage() {
        return "too many arguments: " + command.getSyntaxBuilder().getSyntax() + " does not accept " + args.length + " arguments";
    }

    @Override
    public String getLocalizedMessage() {
        return getMessage();
    }
}
