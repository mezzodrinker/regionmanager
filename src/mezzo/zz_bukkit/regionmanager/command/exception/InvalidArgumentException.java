package mezzo.zz_bukkit.regionmanager.command.exception;

import mezzo.zz_bukkit.regionmanager.command.ArgumentList;
import mezzo.zz_bukkit.regionmanager.command.Command;

/**
 * <code>InvalidArgumentException</code>
 * 
 * @author mezzodrinker
 * @since
 */
public class InvalidArgumentException extends CommandException {
    private final int          index;
    private final Command      command;
    private final ArgumentList argumentList;
    private final String[]     arguments;

    public InvalidArgumentException(Command command, ArgumentList argumentList, String[] arguments, int index) {
        this.command = command;
        this.argumentList = argumentList;
        this.arguments = arguments;
        this.index = index;
    }

    public Command getCommand() {
        return command;
    }

    public ArgumentList getArgumentList() {
        return argumentList;
    }

    public String[] getArguments() {
        return arguments;
    }

    public String getInvalidArgument() {
        return arguments[index];
    }

    @Override
    public String getMessage() {
        return "invalid argument: argument at index " + index + " (" + getInvalidArgument() + ") is invalid";
    }

    @Override
    public String getLocalizedMessage() {
        return getMessage();
    }
}
