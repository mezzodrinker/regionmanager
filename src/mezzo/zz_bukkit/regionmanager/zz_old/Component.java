package mezzo.zz_bukkit.regionmanager.zz_old;

import java.io.IOException;

/**
 * <code>Component</code>
 * 
 * @author mezzodrinker
 */
public interface Component {
    public String getID();

    public void load() throws IOException;

    public void save() throws IOException;
}
