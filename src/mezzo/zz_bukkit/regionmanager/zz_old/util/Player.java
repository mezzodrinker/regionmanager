package mezzo.zz_bukkit.regionmanager.zz_old.util;

import mezzo.bukkit.util.Vector3;
import mezzo.zz_bukkit.regionmanager.zz_old.RegionManagerPlugin;
import mezzo.zz_bukkit.regionmanager.zz_old.exceptions.plugin.permission.MissingGroupSupportException;

import org.bukkit.BanList.Type;

/**
 * <code>Player</code>
 * 
 * @author mezzodrinker
 */
public class Player {
    private org.bukkit.OfflinePlayer offlinePlayer = null;
    private RegionManagerPlugin      plugin        = null;

    public Player(org.bukkit.OfflinePlayer offlinePlayer, RegionManagerPlugin plugin) {
        if (offlinePlayer == null) throw new IllegalArgumentException("player can not be null");
        if (plugin == null) throw new IllegalArgumentException("plugin can not be null");
        this.offlinePlayer = offlinePlayer;
        this.plugin = plugin;
    }

    public String getName() {
        return offlinePlayer.getName();
    }

    public boolean isGroupMember(String group) {
        try {
            return plugin.compatManager().permissionProvider().isGroupMember(this, group);
        } catch (MissingGroupSupportException e) {
            plugin.logger().log("caught " + e.getClass().getSimpleName() + " in isGroupMember(String)", e);
            return false;
        }
    }

    public Vector3 getPosition() {
        if (isOnline()) return Vector3.valueOf(toBukkitPlayer().getLocation());
        return null;
    }

    public void kick(String message) {
        if (isOnline()) {
            toBukkitPlayer().kickPlayer(message);
        }
    }

    public void ban(String message) {
        plugin.getServer().getBanList(Type.NAME).addBan(getName(), message, null, plugin.getName());
        if (isOnline()) {
            toBukkitPlayer().kickPlayer(message);
        }
    }

    public String[] getGroups() {
        try {
            return plugin.compatManager().permissionProvider().getGroups(this);
        } catch (MissingGroupSupportException e) {
            plugin.logger().log("caught " + e.getClass().getSimpleName() + " in getGroups()", e);
            return null;
        }
    }

    public void sendMessage(String message) {
        if (isOnline()) {
            toBukkitPlayer().sendMessage(message);
        }
    }

    public boolean hasPermission(String permission) {
        return plugin.compatManager().permissionProvider().hasPermission(this, permission);
    }

    public org.bukkit.entity.Player toBukkitPlayer() {
        return offlinePlayer.getPlayer();
    }

    public boolean isOnline() {
        return offlinePlayer.isOnline();
    }

    public static Player valueOf(org.bukkit.OfflinePlayer player, RegionManagerPlugin plugin) {
        return new Player(player, plugin);
    }
}
