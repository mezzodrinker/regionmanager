package mezzo.zz_bukkit.regionmanager.zz_old.util;

/**
 * <code>Provider</code>
 * 
 * @author mezzodrinker
 */
public abstract class Provider {
    public abstract String getProviderName();
}
