package mezzo.zz_bukkit.regionmanager.zz_old.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import mezzo.bukkit.util.Vector;
import mezzo.bukkit.util.Vector3;
import mezzo.zz_bukkit.regionmanager.zz_old.region.AccessGroup;
import mezzo.zz_bukkit.regionmanager.zz_old.region.RegionSettings;
import mezzo.zz_bukkit.regionmanager.zz_old.region.flag.Flag;

/**
 * <code>SerializationHelper</code>
 * 
 * @author mezzodrinker
 */
public class SerializationHelper extends mezzo.util.configuration.serialization.SerializationHelper {
    private SerializationHelper() {};

    public static String asString(Map<String, Object> map, String key) {
        Object o = map.get(key);
        if (o == null) throw new IllegalArgumentException("missing obligatory key " + key);
        return String.valueOf(o);
    }

    public static List<String> asStringList(Map<String, Object> map, String key) {
        Object value = map.get(key);
        if (value == null) throw new IllegalArgumentException("missing obligatory key " + key);
        if (!(value instanceof List)) throw new IllegalArgumentException(key + "(" + value + ") is not a string list");
        List<?> l = (List<?>) value;
        List<String> ret = new ArrayList<String>();
        for (Object o : l) {
            ret.add(String.valueOf(o));
        }
        return ret;
    }

    @SuppressWarnings("unchecked")
    public static Vector3 asVector3(Map<String, Object> map, String key) {
        Object o = map.get(key);
        if (o == null) throw new IllegalArgumentException("missing obligatory key " + key);
        if (o instanceof Map) return Vector3.deserialize((Map<String, Object>) o);
        if (!(o instanceof Vector)) throw new IllegalArgumentException(key + "(" + o + ") is not a vector");
        Vector v = (Vector) o;
        return new Vector3(v.getX(), v.getY(), v.getZ());
    }

    @SuppressWarnings("unchecked")
    public static RegionSettings asRegionSettings(Map<String, Object> map, String key) {
        Object o = map.get(key);
        if (o == null) throw new IllegalArgumentException("missing obligatory key " + key);
        if (o instanceof Map) return RegionSettings.deserialize((Map<String, Object>) o);
        if (!(o instanceof RegionSettings)) throw new IllegalArgumentException(key + "(" + o + ") is not an instance of RegionSettings");
        return (RegionSettings) o;
    }

    public static int asInteger(Map<String, Object> map, String key) {
        Object o = map.get(key);
        if (o == null) throw new IllegalArgumentException("missing obligatory key " + key);
        if (o instanceof String) {
            String s = String.valueOf(o);
            try {
                return Double.valueOf(s).intValue();
            } catch (NumberFormatException e) {
            }
        }
        if (!(o instanceof Number)) throw new IllegalArgumentException(key + "(" + o + ") is not a number");
        return ((Number) o).intValue();
    }

    public static Map<? extends Flag<?>, Object> asFlagList(Map<String, Object> map, String key) {
        Object o = map.get(key);
        if (o == null) throw new IllegalArgumentException("missing obligatory key " + key);
        if (!(o instanceof Map)) throw new IllegalArgumentException(key + "(" + o + ") is not a map");
        Map<?, ?> m = (Map<?, ?>) o;
        Map<Flag<?>, Object> ret = new HashMap<Flag<?>, Object>();
        for (Entry<?, ?> entry : m.entrySet()) {
            if (!(entry.getKey() instanceof Flag)) {
                continue;
            }
            Flag<?> f = (Flag<?>) entry.getKey();
            Object v = null;
            try {
                v = f.cast(entry.getValue());
            } catch (ClassCastException e) {
                continue;
            }
            ret.put(f, v);
        }
        return ret;
    }

    public static AccessGroup asAccessGroup(Map<String, Object> map, String key) {
        Object o = map.get(key);
        if (o == null) throw new IllegalArgumentException("missing obligatory key " + key);
        if (o instanceof Map) return AccessGroup.deserialize(map);
        if (!(o instanceof AccessGroup)) throw new IllegalArgumentException(key + "(" + o + ") is not an instance of AccessGroup");
        return (AccessGroup) o;
    }
}
