package mezzo.zz_bukkit.regionmanager.zz_old.permission;

import mezzo.zz_bukkit.regionmanager.zz_old.RegionManagerPlugin;
import mezzo.zz_bukkit.regionmanager.zz_old.command.Command;
import mezzo.zz_bukkit.regionmanager.zz_old.util.Player;

import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

/**
 * <code>PermissionManager</code>
 * 
 * @author mezzodrinker
 */
public class PermissionManager {
    private RegionManagerPlugin plugin                             = null;

    private static final String PERM_ROOT                          = "regionmanager.";
    private static final String PERM_COMMAND                       = PERM_ROOT + "command.";
    private static final String PERM_COMMAND_ADMIN                 = PERM_COMMAND + "admin.";

    // Permissions list
    // TODO split into static final subclasses
    public static final String  PERM_INFO                          = PERM_ROOT + "info";
    public static final String  PERM_COMMAND_HELP                  = PERM_COMMAND + "help";
    public static final String  PERM_COMMAND_CONFIG                = PERM_COMMAND + "config";
    public static final String  PERM_COMMAND_CONFIG_HELP           = PERM_COMMAND_CONFIG + ".help";
    public static final String  PERM_COMMAND_REGION                = PERM_COMMAND + "region";
    public static final String  PERM_COMMAND_REGION_ADD            = PERM_COMMAND_REGION + ".add";
    public static final String  PERM_COMMAND_REGION_DELETE         = PERM_COMMAND_REGION + ".delete";
    public static final String  PERM_COMMAND_REGION_MEMBERS        = PERM_COMMAND_REGION + ".members";
    public static final String  PERM_COMMAND_REGION_MEMBERS_ADD    = PERM_COMMAND_REGION_MEMBERS + ".add";
    public static final String  PERM_COMMAND_REGION_MEMBERS_REMOVE = PERM_COMMAND_REGION_MEMBERS + ".remove";
    public static final String  PERM_COMMAND_REGION_OWNERS         = PERM_COMMAND_REGION + ".members";
    public static final String  PERM_COMMAND_REGION_OWNERS_ADD     = PERM_COMMAND_REGION_OWNERS + ".add";
    public static final String  PERM_COMMAND_REGION_OWNERS_REMOVE  = PERM_COMMAND_REGION_OWNERS + ".remove";
    public static final String  PERM_COMMAND_ADMIN_SAVE            = PERM_COMMAND_ADMIN + "save";
    public static final String  PERM_COMMAND_ADMIN_RELOAD          = PERM_COMMAND_ADMIN + "reload";

    public PermissionManager(RegionManagerPlugin plugin) {
        this.plugin = plugin;
    }

    public boolean canExecute(CommandSender sender, Command command) {
        if (sender == null || command == null) return false;
        if (sender instanceof ConsoleCommandSender) return true;
        if (sender instanceof org.bukkit.entity.Player)
            return plugin.compatManager().permissionProvider().hasPermission(Player.valueOf((org.bukkit.entity.Player) sender, plugin), command.getPermission());
        return sender.hasPermission(command.getPermission());
    }
}
