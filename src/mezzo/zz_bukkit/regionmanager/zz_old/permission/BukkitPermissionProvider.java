package mezzo.zz_bukkit.regionmanager.zz_old.permission;

import mezzo.zz_bukkit.regionmanager.zz_old.exceptions.plugin.permission.MissingGroupSupportException;
import mezzo.zz_bukkit.regionmanager.zz_old.util.Player;

import org.bukkit.World;

/**
 * <code>BukkitPermissionProvider</code>
 * 
 * @author mezzodrinker
 */
public class BukkitPermissionProvider extends PermissionProvider {
    private void checkGroupSupport() throws MissingGroupSupportException {
        throw new MissingGroupSupportException("Bukkit does not support groups");
    }

    @Override
    public final String getProviderName() {
        return "Bukkit";
    }

    @Override
    public final boolean hasGroupSupport() {
        return false;
    }

    @Override
    public boolean hasPermission(Player player, String permission) {
        return player.toBukkitPlayer().hasPermission(permission);
    }

    @Override
    public boolean isGroupMember(Player player, String group) throws MissingGroupSupportException {
        checkGroupSupport();
        return false;
    }

    @Override
    public String[] getGroups(Player player) throws MissingGroupSupportException {
        checkGroupSupport();
        return null;
    }

    @Override
    public String getPrimaryGroup(Player player) throws MissingGroupSupportException {
        checkGroupSupport();
        return null;
    }

    @Override
    public boolean hasPermission(String group, String permission, World world) throws MissingGroupSupportException {
        checkGroupSupport();
        return false;
    }
}
