package mezzo.zz_bukkit.regionmanager.zz_old.permission;

import mezzo.zz_bukkit.regionmanager.zz_old.exceptions.plugin.permission.MissingGroupSupportException;
import mezzo.zz_bukkit.regionmanager.zz_old.util.Player;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.World;
import org.bukkit.plugin.Plugin;

/**
 * <code>VaultPermissionProvider</code>
 * 
 * @author mezzodrinker
 */
public class VaultPermissionProvider extends PermissionProvider {
    private Plugin     plugin     = null;
    private Permission permission = null;

    public VaultPermissionProvider(Plugin plugin, Permission permission) {
        if (plugin == null) throw new IllegalArgumentException("plugin can not be null");
        if (permission == null) throw new IllegalArgumentException("permission can not be null");
        this.plugin = plugin;
        this.permission = permission;
    }

    private void checkGroupSupport() throws MissingGroupSupportException {
        if (!hasGroupSupport()) throw new MissingGroupSupportException(getProviderName() + " does not support groups");
    }

    public Plugin getPlugin() {
        return plugin;
    }

    @Override
    public String getProviderName() {
        return plugin.getName();
    }

    @Override
    public boolean hasGroupSupport() {
        return permission.hasGroupSupport();
    }

    @Override
    public boolean hasPermission(Player player, String permission) {
        if (this.permission.has(player.toBukkitPlayer(), permission)) return true;
        try {
            for (String s : getGroups(player)) {
                if (hasPermission(s, permission, player.toBukkitPlayer().getWorld())) return true;
            }
        } catch (MissingGroupSupportException e) {
        }
        return false;
    }

    @Override
    public boolean isGroupMember(Player player, String group) throws MissingGroupSupportException {
        checkGroupSupport();
        return permission.playerInGroup(player.toBukkitPlayer(), group);
    }

    @Override
    public String[] getGroups(Player player) throws MissingGroupSupportException {
        checkGroupSupport();
        return permission.getPlayerGroups(player.toBukkitPlayer());
    }

    @Override
    public String getPrimaryGroup(Player player) throws MissingGroupSupportException {
        checkGroupSupport();
        return permission.getPrimaryGroup(player.toBukkitPlayer());
    }

    @Override
    public boolean hasPermission(String group, String permission, World world) throws MissingGroupSupportException {
        checkGroupSupport();
        return this.permission.groupHas(world, group, permission);
    }
}
