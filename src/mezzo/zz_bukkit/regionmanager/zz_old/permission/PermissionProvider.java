package mezzo.zz_bukkit.regionmanager.zz_old.permission;

import mezzo.zz_bukkit.regionmanager.zz_old.exceptions.plugin.permission.MissingGroupSupportException;
import mezzo.zz_bukkit.regionmanager.zz_old.util.Player;
import mezzo.zz_bukkit.regionmanager.zz_old.util.Provider;

import org.bukkit.World;

/**
 * <code>PermissionProvider</code>
 * 
 * @author mezzodrinker
 */
public abstract class PermissionProvider extends Provider {
    public abstract boolean hasGroupSupport();

    public abstract boolean hasPermission(Player player, String permission);

    public abstract boolean hasPermission(String group, String permission, World world) throws MissingGroupSupportException;

    public abstract boolean isGroupMember(Player player, String group) throws MissingGroupSupportException;

    public abstract String[] getGroups(Player player) throws MissingGroupSupportException;

    public abstract String getPrimaryGroup(Player player) throws MissingGroupSupportException;
}
