package mezzo.zz_bukkit.regionmanager.zz_old.world;

import static mezzo.zz_bukkit.regionmanager.zz_old.configuration.ConfigManager.CONFIG_MAIN_WORLDS_DISABLED;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import mezzo.zz_bukkit.regionmanager.zz_old.Component;
import mezzo.zz_bukkit.regionmanager.zz_old.RegionManagerPlugin;
import mezzo.zz_bukkit.regionmanager.zz_old.exceptions.world.WorldDisabledException;
import mezzo.zz_bukkit.regionmanager.zz_old.region.RegionManager;

import org.bukkit.World;

/**
 * <code>WorldManager</code>
 * 
 * @author mezzodrinker
 */
public class WorldManager implements Component {
    private RegionManagerPlugin       plugin         = null;
    private List<World>               worlds         = new ArrayList<World>();
    private Map<World, RegionManager> regionManagers = new LinkedHashMap<World, RegionManager>();

    public final File                 folderWorlds;

    public WorldManager(RegionManagerPlugin plugin) {
        this.plugin = plugin;
        folderWorlds = plugin.getFile("worlds");
    }

    private void checkIfWorldEnabled(World world) throws WorldDisabledException {
        if (!isWorldEnabled(world)) throw new WorldDisabledException(world);
    }

    private void checkIfWorldEnabled(String world) throws WorldDisabledException {
        if (!isWorldEnabled(world)) throw new WorldDisabledException(world);
    }

    public void loadWorlds() {
        List<String> disabledWorlds = plugin.configManager().config().getStringList(CONFIG_MAIN_WORLDS_DISABLED);
        List<Pattern> patterns = new ArrayList<Pattern>();

        if (disabledWorlds != null) {
            for (String world : disabledWorlds) {
                Pattern p = Pattern.compile(world);
                if (!disabledWorlds.contains(p)) {
                    patterns.add(p);
                }
            }
        }

        for (World world : plugin.getServer().getWorlds()) {
            String name = world.getName();
            for (Pattern p : patterns) {
                if (p.matcher(name).matches() && !worlds.contains(world)) {
                    worlds.add(world);
                    regionManagers.put(world, new RegionManager(plugin, world));
                    break;
                }
            }
        }
    }

    public boolean isWorldEnabled(World world) {
        return worlds.contains(world);
    }

    public boolean isWorldEnabled(String name) {
        return worlds.contains(plugin.getServer().getWorld(name));
    }

    public RegionManager regionManager(String world) throws WorldDisabledException {
        checkIfWorldEnabled(world);
        return regionManagers.get(plugin.getServer().getWorld(world));
    }

    public RegionManager regionManager(World world) throws WorldDisabledException {
        checkIfWorldEnabled(world);
        return regionManagers.get(world);
    }

    public RegionManager[] regionManagers() {
        return regionManagers.values().toArray(new RegionManager[0]);
    }

    @Override
    public String getID() {
        return "regions";
    }

    @Override
    public void load() throws IOException {
        loadWorlds();
    }

    @Override
    public void save() throws IOException {
        for (RegionManager regionManager : regionManagers()) {
            regionManager.save();
        }
    }
}
