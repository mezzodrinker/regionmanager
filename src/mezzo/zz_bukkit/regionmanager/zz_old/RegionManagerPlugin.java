package mezzo.zz_bukkit.regionmanager.zz_old;

import static mezzo.zz_bukkit.regionmanager.zz_old.configuration.ConfigManager.CONFIG_MAIN_LANG;
import static mezzo.zz_bukkit.regionmanager.zz_old.configuration.ConfigManager.CONFIG_MAIN_UPDATER_DOWNLOAD;
import static mezzo.zz_bukkit.regionmanager.zz_old.configuration.ConfigManager.CONFIG_MAIN_UPDATER_PERFORMCHECKS;
import static mezzo.zz_bukkit.regionmanager.zz_old.message.MessageManager.replacement;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mezzo.bukkit.BukkitPlugin;
import mezzo.bukkit.logging.DefaultPluginLogger;
import mezzo.bukkit.logging.FilePluginLogger;
import mezzo.bukkit.logging.IPluginLogger;
import mezzo.bukkit.net.Updater;
import mezzo.bukkit.net.Updater.UpdateType;
import mezzo.util.configuration.yaml.YamlConfiguration;
import mezzo.util.time.StopWatch;
import mezzo.util.version.Version;
import mezzo.zz_bukkit.regionmanager.zz_old.command.Command;
import mezzo.zz_bukkit.regionmanager.zz_old.command.CommandManager;
import mezzo.zz_bukkit.regionmanager.zz_old.compatibility.CompatManager;
import mezzo.zz_bukkit.regionmanager.zz_old.configuration.ConfigManager;
import mezzo.zz_bukkit.regionmanager.zz_old.configuration.ConfigUpdater;
import mezzo.zz_bukkit.regionmanager.zz_old.message.MessageManager;
import mezzo.zz_bukkit.regionmanager.zz_old.message.Messages;
import mezzo.zz_bukkit.regionmanager.zz_old.message.Variables;
import mezzo.zz_bukkit.regionmanager.zz_old.permission.PermissionManager;
import mezzo.zz_bukkit.regionmanager.zz_old.world.WorldManager;

/**
 * <code>RegionManagerPlugin</code>
 * 
 * @author mezzodrinker
 */
public class RegionManagerPlugin extends BukkitPlugin implements Component {
    private boolean                loadSuccess       = false;
    private IPluginLogger          logger            = null;
    private CommandManager         commandManager    = null;
    private ConfigManager          configManager     = null;
    private ConfigUpdater          configUpdater     = null;
    private MessageManager         messageManager    = null;
    private CompatManager          compatManager     = null;
    private PermissionManager      permissionManager = null;
    private WorldManager           worldManager      = null;
    private StopWatch              stopWatch         = null;
    private List<Component>        components        = new ArrayList<Component>();

    protected static int           cursePluginID     = 54103;
    protected static ReleaseStatus releaseStatus     = ReleaseStatus.DEV;

    public static final String     URL_CONFIG_HELP   = "http://dev.bukkit.org/bukkit-plugins/regionmanager/pages/configuration/";

    public RegionManagerPlugin() {
        this(cursePluginID);
    }

    protected RegionManagerPlugin(int pluginID) {
        super(pluginID);
        try {
            logger = new FilePluginLogger(this);
        } catch (IOException e) {
            logger = new DefaultPluginLogger(this);
            logger.log("Could not initialize FilePluginLogger", e);
        }
    }

    private void checkIfRelease() {
        if (releaseStatus != ReleaseStatus.RELEASE) {
            logger().warning(
                    "You are using " + (releaseStatus == ReleaseStatus.ALPHA ? "an " : "a ") + releaseStatus.toString().toUpperCase() + " build of RegionManager. This "
                            + (releaseStatus == ReleaseStatus.BETA ? "might" : "will") + " result in serious stability issues. Use at your own risk!");
        }
    }

    private void startTimer() {
        if (stopWatch == null) {
            stopWatch = new StopWatch();
        }

        stopWatch.stop();
        stopWatch.start();
    }

    private long stopTimer() {
        if (stopWatch == null) return 0;

        stopWatch.stop();
        return stopWatch.getElapsedTime();
    }

    /**
     * Removes outdated files from the plugins folder.
     */
    private void removeFiles() {
        logger().info("removing outdated versions from plugins folder");

        File self = this.getFile();
        File[] others = self.getParentFile().listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().contains("regionmanager") && name.toLowerCase().contains(".jar") && !name.equalsIgnoreCase(self.getName());
            }
        });

        // STEP 1:
        // check if other files in the plugins folder that contain "regionmanager" in their names contain a version number that's absolutely smaller than the current one
        if (others != null && others.length > 0) {
            logger().debug("[Step 1] scanning for outdated versions (filename)");
            Version version = getVersion();
            int counter = 0;
            for (File file : others) {
                Version v = Version.valueOf(file.getName().replace(".jar", "").replaceAll("[^0-9\\.]", ""));
                if (v.compareTo(version) <= 0) {
                    logger().debug("[Step 1] deleting " + file.getName());
                    file.delete();
                    counter++;
                }
            }
            if (counter > 0) {
                logger().debug("[Step 1] found and deleted " + counter + " outdated " + (counter == 1 ? "version" : "versions"));
            } else {
                logger().debug("[Step 1] no outdated versions detected");
            }
        } else {
            logger().debug("[Step 1] no outdated versions detected");
        }

        others = self.getParentFile().listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().contains(".jar") && !name.equalsIgnoreCase(self.getName());
            }
        });

        // STEP 2:
        // check if other .jar files in the plugins folder contain a plugin.yml with name equaling "regionmanager" and a version that's smaller than the current one
        if (others != null && others.length > 0) {
            logger().debug("[Step 2] scanning for outdated versions (plugin.yml)");
            Version version = getVersion();
            int counter = 0;
            for (File file : others) {
                try {
                    JarFile jar = new JarFile(file);
                    Enumeration<JarEntry> entries = jar.entries();
                    boolean remove = false;
                    boolean skip = false;

                    while (entries.hasMoreElements()) {
                        JarEntry entry = entries.nextElement();
                        if (entry.getName().equalsIgnoreCase("plugin.yml")) {
                            InputStream in = jar.getInputStream(entry);
                            YamlConfiguration c = YamlConfiguration.loadConfiguration(in);

                            String name = c.getString("name");
                            Version v = Version.valueOf(c.getString("version"));

                            if (name.equalsIgnoreCase("regionmanager")) {
                                if (v.compareTo(version) < 0) {
                                    remove = true;
                                    break;
                                }
                                logger().debug("skipping file " + file.getName() + ": version " + v + " is not outdated");
                                skip = true;
                                break;
                            }
                            logger().debug("skipping file " + file.getName() + ": wrong plugin");
                            skip = true;
                            break;
                        }
                        logger().debug("skipping entry " + entry.getName() + ": is not plugin.yml");
                    }

                    jar.close();

                    if (skip) {
                        continue;
                    }

                    if (remove) {
                        logger.debug("removing " + file.getName());
                        file.delete();
                        counter++;
                        continue;
                    }

                    logger().debug("skipping file " + file.getName());
                } catch (IOException e) {
                    logger().log(file.getName() + " could not be loaded as a .jar (maybe it is not a .jar file?)", e);
                }
            }

            if (counter > 0) {
                logger().debug("[Step 2] found and deleted " + counter + " outdated " + (counter == 1 ? "version" : "versions"));
            } else {
                logger().debug("[Step 2] no outdated versions detected");
            }
        } else {
            logger.debug("no other files found in plugins folder");
        }

        logger().info("done");
    }

    @Override
    public void onEnable() {
        // notify the server's owner of stability issues with DEV, ALPHA and BETA versions
        checkIfRelease();

        Throwable t = null;

        // try to load regionmanager
        try {
            loadSuccess = false;
            load();
        } catch (IOException e) {
            t = e;
        }

        // if the load didn't succeed, disable regionmanager
        if (!loadSuccess || t != null) {
            logger().log("an error occurred while loading, disabling regionmanager", t);
            getPluginLoader().disablePlugin(this);
            return;
        }
    }

    @Override
    public void onDisable() {
        try {
            save();
        } catch (IOException e) {
            logger.log(Messages.Disable.failed, e);
        }

        // show that RegionManager has been successfully disabled
        logger().info(messageManager.messageWrapped(Messages.Disable.succeeded));
    }

    @Override
    public void load() throws IOException {
        // start the timer for run time statistics
        startTimer();

        // initialize and load configuration manager
        logger().info("initializing and loading configuration manager");
        configManager = new ConfigManager(this);
        try {
            configManager.loadConfig();
            components.add(configManager);
        } catch (IOException e) {
            logger().log("could not load configurations", e);
            logger().warning("fatal error, disabling regionmanager");
            getPluginLoader().disablePlugin(this);
            return;
        }

        // initialize and load configuration updater
        logger().info("initializing and loading configuration updater");
        configUpdater = new ConfigUpdater(this);
        try {
            configUpdater.updateConfigs();
        } catch (IOException e) {
            logger().log("could not update configurations", e);
            logger().warning("fatal error, disabling regionmanager");
            getPluginLoader().disablePlugin(this);
            return;
        }

        // initialize and load message manager
        logger().info("initializing and loading message manager");
        messageManager = new MessageManager();
        try {
            messageManager.loadLanguage(configManager.config().getString(CONFIG_MAIN_LANG));
            components.add(messageManager);
        } catch (IOException e) {
            logger().log("could not load messages", e);
            logger().log("retrying with default language");
            try {
                messageManager.loadLanguage(MessageManager.DEFAULT_LANGUAGE);
            } catch (IOException e2) {
                logger().log("could not load messages", e2);
                logger().warning("fatal error, disabling regionmanager");
                getPluginLoader().disablePlugin(this);
                return;
            }
        }

        // initialize and load update checker
        logger().info(messageManager.messageWrapped(Messages.Updater.UpdateCheck.initializing));
        updateChecker = new Updater(this, cursePluginID, UpdateType.NO_UPDATE);

        // initialize and load updater
        logger().info(messageManager.messageWrapped(Messages.Updater.Update.initializing));
        updater = new Updater(this, cursePluginID, UpdateType.CHECK_AND_UPDATE);

        // perform update check
        if (configManager.config().getBoolean(CONFIG_MAIN_UPDATER_PERFORMCHECKS) && !configManager.config().getBoolean(CONFIG_MAIN_UPDATER_DOWNLOAD)) {
            logger().info(messageManager.messageWrapped(Messages.Updater.UpdateCheck.started));
            updateChecker.check();
        } else {
            logger().info(messageManager.messageWrapped(Messages.Updater.UpdateCheck.disabled));
        }

        // download updates
        if (configManager.config().getBoolean(CONFIG_MAIN_UPDATER_DOWNLOAD)) {
            logger().info(messageManager.messageWrapped(Messages.Updater.Update.started));
            updater.check();
        } else {
            logger().info(messageManager.messageWrapped(Messages.Updater.Update.disabled));
        }

        // load files
        logger().info(messageManager.messageWrapped(Messages.AdditionalFiles.initializing));

        // initialize and load compatibility manager
        logger().info(messageManager.messageWrapped(Messages.CompatManager.initializing));
        compatManager = new CompatManager(this);
        compatManager.load();
        components.add(compatManager);

        // initialize and load permission manager
        logger().info(messageManager.messageWrapped(Messages.PermissionManager.initializing));
        permissionManager = new PermissionManager(this);

        // initialize and load command manager
        logger().info(messageManager.messageWrapped(Messages.CommandManager.initializing));
        commandManager = new CommandManager(this);
        getCommand(Command.REGIONMANAGER.getLabel()).setExecutor(commandManager);

        // initialize and load world manager
        logger().info(messageManager.messageWrapped(Messages.WorldManager.initializing));
        worldManager = new WorldManager(this);
        worldManager.loadWorlds();
        components.add(worldManager);

        // initialize and load sign manager
        logger().info(messageManager.messageWrapped(Messages.SignManager.initializing));

        // initialize and load task manager
        logger().info(messageManager.messageWrapped(Messages.TaskManager.initializing));

        // initialize and load event manager
        logger().info(messageManager.messageWrapped(Messages.EventManager.initializing));

        // initialize and load trade manager
        logger().info(messageManager.messageWrapped(Messages.TradeManager.initializing));

        // initialize and load seize manager
        logger().info(messageManager.messageWrapped(Messages.SeizeManager.initializing));

        // show that RegionManager has been successfully enabled
        long time = stopTimer();
        logger().info(messageManager.messageWrapped(Messages.Enable.succeeded));
        logger().debug(messageManager.messageWrapped(Messages.Enable.elapsed_time, replacement(Variables.time, time)));

        // remove outdated files of RegionManager from the plugins folder
        removeFiles();

        // set loadSuccess to true
        loadSuccess = true;
    }

    @Override
    public void save() throws IOException {
        // save configurations
        logger().info(messageManager.messageWrapped(Messages.Save.started_server));
        try {
            configManager().saveConfig();
        } catch (IOException e) {
            logger().log(messageManager.messageWrapped(Messages.Save.failed), e);
        }
        logger().info(messageManager.messageWrapped(Messages.Save.succeeded));
    }

    @Override
    public CommandManager commandManager() {
        return commandManager;
    }

    @Override
    public ConfigManager configManager() {
        return configManager;
    }

    public ConfigUpdater configUpdater() {
        return configUpdater;
    }

    public MessageManager messageManager() {
        return messageManager;
    }

    public CompatManager compatManager() {
        return compatManager;
    }

    public PermissionManager permissionManager() {
        return permissionManager;
    }

    public WorldManager worldManager() {
        return worldManager;
    }

    public Updater updateChecker() {
        return updateChecker;
    }

    @Override
    public IPluginLogger logger() {
        return logger;
    }

    public void addComponent(Component c) {
        if (!components.contains(c)) {
            components.add(c);
        }
    }

    public Component getComponent(String name) {
        for (Component c : components) {
            if (c.getID().equals(name)) return c;
        }
        return null;
    }

    public List<Component> getComponents(String regex) {
        List<Component> l = new ArrayList<Component>();
        Pattern pattern = Pattern.compile(regex);
        for (Component c : components) {
            Matcher m = pattern.matcher(c.getID());
            if (m.matches()) {
                l.add(c);
            }
        }
        return l;
    }

    @Override
    public Version getVersion() {
        return Version.valueOf(getDescription().getVersion());
    }

    @Override
    public String getID() {
        return "all";
    }
}
