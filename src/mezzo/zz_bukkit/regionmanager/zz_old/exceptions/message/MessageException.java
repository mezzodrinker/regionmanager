package mezzo.zz_bukkit.regionmanager.zz_old.exceptions.message;

/**
 * <code>MessageException</code>
 * 
 * @author mezzodrinker
 */
public abstract class MessageException extends Exception {
    private String            messageKey       = null;
    private String            language         = null;

    private static final long serialVersionUID = 3462145490980263127L;

    protected MessageException(String messageKey, String language) {
        if (messageKey == null || messageKey.isEmpty()) throw new IllegalArgumentException("message key can not be null or empty");
        if (language == null || language.isEmpty()) throw new IllegalArgumentException("language can not be null or empty");

        this.messageKey = messageKey;
        this.language = language;
    }

    public String getMessageKey() {
        return messageKey;
    }

    public String getLanguage() {
        return language;
    }

    @Override
    public abstract String getMessage();
}
