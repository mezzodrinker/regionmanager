package mezzo.zz_bukkit.regionmanager.zz_old.exceptions.message;

/**
 * <code>MissingMessageException</code>
 * 
 * @author mezzodrinker
 */
public class MissingMessageException extends MessageException {
    private String            message          = null;

    private static final long serialVersionUID = 1951130618113196978L;

    public MissingMessageException(String messageKey, String language) {
        super(messageKey, language);
    }

    public MissingMessageException(String messageKey, String language, String message) {
        super(messageKey, language);
        this.message = message;
    }

    @Override
    public String getMessage() {
        if (message == null) return "language " + getLanguage() + " is missing the key " + getMessageKey();
        return message;
    }
}
