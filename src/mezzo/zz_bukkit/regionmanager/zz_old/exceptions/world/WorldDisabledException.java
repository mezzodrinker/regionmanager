package mezzo.zz_bukkit.regionmanager.zz_old.exceptions.world;

import org.bukkit.World;

/**
 * <code>WorldDisabledException</code>
 * 
 * @author mezzodrinker
 */
public class WorldDisabledException extends Exception {
    private String            world            = null;
    private String            message          = null;

    private static final long serialVersionUID = 1039384015875621728L;

    public WorldDisabledException(String world) {
        this.world = world;
    }

    public WorldDisabledException(World world) {
        this(world.getName());
    }

    public WorldDisabledException(String world, String message) {
        this(world);
        this.message = message;
    }

    public WorldDisabledException(World world, String message) {
        this(world.getName(), message);
    }

    public String getWorld() {
        return world;
    }

    @Override
    public String getMessage() {
        return message == null ? "the world '" + world + "' has been disabled by configuration" : message;
    }
}
