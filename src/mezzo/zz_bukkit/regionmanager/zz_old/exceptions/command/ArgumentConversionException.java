package mezzo.zz_bukkit.regionmanager.zz_old.exceptions.command;

import mezzo.zz_bukkit.regionmanager.zz_old.command.Command;

/**
 * <code>ArgumentConversionException</code>
 * 
 * @author mezzodrinker
 */
public class ArgumentConversionException extends CommandException {
    private final Object      subject;
    private final Class<?>    original;
    private final Class<?>    conversionTo;

    private static final long serialVersionUID = -4970043559180750525L;

    public ArgumentConversionException(Command command, Class<?> original, Class<?> conversionTo) {
        super(command);
        subject = null;
        this.original = original;
        this.conversionTo = conversionTo;
    }

    public ArgumentConversionException(Command command, Object subject, Class<?> original, Class<?> conversionTo) {
        super(command);
        this.subject = subject;
        this.original = original;
        this.conversionTo = conversionTo;
    }

    public ArgumentConversionException(Command command, String message, Class<?> original, Class<?> conversionTo) {
        super(command, message);
        subject = null;
        this.original = original;
        this.conversionTo = conversionTo;
    }

    public ArgumentConversionException(Command command, String message, Object subject, Class<?> original, Class<?> conversionTo) {
        super(command, message);
        this.subject = subject;
        this.original = original;
        this.conversionTo = conversionTo;
    }

    public ArgumentConversionException(Command command, Class<?> original, Class<?> conversionTo, Throwable cause) {
        super(command, cause);
        subject = null;
        this.original = original;
        this.conversionTo = conversionTo;
    }

    public ArgumentConversionException(Command command, Object subject, Class<?> original, Class<?> conversionTo, Throwable cause) {
        super(command, cause);
        this.subject = subject;
        this.original = original;
        this.conversionTo = conversionTo;
    }

    public ArgumentConversionException(Command command, String message, Class<?> original, Class<?> conversionTo, Throwable cause) {
        super(command, message, cause);
        subject = null;
        this.original = original;
        this.conversionTo = conversionTo;
    }

    public ArgumentConversionException(Command command, String message, Object subject, Class<?> original, Class<?> conversionTo, Throwable cause) {
        super(command, message, cause);
        this.subject = subject;
        this.original = original;
        this.conversionTo = conversionTo;
    }

    public Object getSubject() {
        return subject;
    }

    public Class<?> getOriginalClass() {
        return original;
    }

    public Class<?> getConversionClass() {
        return conversionTo;
    }

    @Override
    public String getMessage() {
        return message == null ? "can't convert " + (subject == null ? "" : subject + " ") + "from " + original.getName() + " to " + conversionTo.getName() : message;
    }
}
