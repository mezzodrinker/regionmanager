package mezzo.zz_bukkit.regionmanager.zz_old.exceptions.command;

import mezzo.zz_bukkit.regionmanager.zz_old.command.Command;

/**
 * <code>CommandException</code>
 * 
 * @author mezzodrinker
 */
public abstract class CommandException extends Exception {
    protected Command         command          = null;
    protected String          message          = null;

    private static final long serialVersionUID = 1047942566159524998L;

    protected CommandException(Command command) {
        if (command == null && !(this instanceof UnknownCommandException)) throw new IllegalArgumentException("command can not be null");
        this.command = command;
    }

    protected CommandException(Command command, String message) {
        this(command);
        this.message = message;
    }

    protected CommandException(Command command, Throwable cause) {
        super(cause);
        if (command == null && !(this instanceof UnknownCommandException)) throw new IllegalArgumentException("command can not be null");
        this.command = command;
    }

    protected CommandException(Command command, String message, Throwable cause) {
        this(command, cause);
        this.message = message;
    }

    public Command getCommand() {
        return command;
    }

    @Override
    public String getMessage() {
        return message == null ? "error executing command " + command.getLabel() : message;
    }
}
