package mezzo.zz_bukkit.regionmanager.zz_old.exceptions.command;

import java.util.Arrays;

import mezzo.zz_bukkit.regionmanager.zz_old.command.Command;

/**
 * <code>TooManyArgumentsException</code>
 * 
 * @author mezzodrinker
 */
public class TooManyArgumentsException extends CommandException {
    private int               argumentCount        = 0;
    private int[]             allowedArgumentCount = null;

    private static final long serialVersionUID     = -2914994073231782748L;

    public TooManyArgumentsException(Command command, int argumentCount, int[] allowedArgumentCount) {
        super(command);
        this.argumentCount = argumentCount;
        this.allowedArgumentCount = allowedArgumentCount;
    }

    public TooManyArgumentsException(Command command, String message, int argumentCount, int[] allowedArgumentCount) {
        super(command, message);
        this.argumentCount = argumentCount;
        this.allowedArgumentCount = allowedArgumentCount;
    }

    public int getArgumentCount() {
        return argumentCount;
    }

    public int[] getAllowedArgumentCount() {
        return allowedArgumentCount;
    }

    @Override
    public String getMessage() {
        return message == null ? "too many arguments - argument count: " + argumentCount + ", allowed: " + Arrays.toString(allowedArgumentCount) : message;
    }
}
