package mezzo.zz_bukkit.regionmanager.zz_old.exceptions.command;

import mezzo.zz_bukkit.regionmanager.zz_old.command.Command;

/**
 * <code>UnimplementedCommandException</code>
 * 
 * @author mezzodrinker
 * @since
 */
public class UnimplementedCommandException extends CommandException {
    private static final long serialVersionUID = 1L;

    public UnimplementedCommandException(Command command) {
        super(command);
    }

    public UnimplementedCommandException(Command command, String message) {
        super(command, message);
    }

    @Override
    public String getMessage() {
        return message == null ? "command " + command.getLabel() + " has not been implemented yet" : message;
    }
}
