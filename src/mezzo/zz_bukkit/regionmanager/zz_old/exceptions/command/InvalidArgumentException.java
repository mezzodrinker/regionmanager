package mezzo.zz_bukkit.regionmanager.zz_old.exceptions.command;

import mezzo.zz_bukkit.regionmanager.zz_old.command.Command;

/**
 * <code>InvalidArgumentException</code>
 * 
 * @author mezzodrinker
 */
public class InvalidArgumentException extends CommandException {
    private final int         argPos;
    private final Object      value;
    private final String      expected;

    private static final long serialVersionUID = -4179123204426161713L;

    public InvalidArgumentException(Command command, int argPos, Object value, String expected) {
        super(command);
        this.argPos = argPos;
        this.value = value;
        this.expected = expected;
    }

    public InvalidArgumentException(Command command, String message, int argPos, Object value, String expected) {
        super(command, message);
        this.argPos = argPos;
        this.value = value;
        this.expected = expected;
    }

    public int getArgPos() {
        return argPos;
    }

    public Object getValue() {
        return value;
    }

    public String getExpected() {
        return expected;
    }

    @Override
    public String getMessage() {
        return message == null ? "invalid argument \"" + value + "\" at position " + argPos + ", expected " + expected : message;
    }
}
