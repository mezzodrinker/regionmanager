package mezzo.zz_bukkit.regionmanager.zz_old.exceptions.command;

/**
 * <code>UnknownCommandException</code>
 * 
 * @author mezzodrinker
 */
public class UnknownCommandException extends CommandException {
    private String            label            = null;

    private static final long serialVersionUID = -8378564549423709173L;

    public UnknownCommandException(String label) {
        super(null);
        this.label = label;
    }

    public UnknownCommandException(String label, String message) {
        super(null, message);
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public String getMessage() {
        return message == null ? "unknown command \'" + label + "\'" : message;
    }
}
