package mezzo.zz_bukkit.regionmanager.zz_old.exceptions.command;

import java.util.Arrays;

import mezzo.zz_bukkit.regionmanager.zz_old.command.Command;

/**
 * <code>TooFewArgumentsException</code>
 * 
 * @author mezzodrinker
 */
public class TooFewArgumentsException extends CommandException {
    private int               argumentCount        = 0;
    private int[]             allowedArgumentCount = null;

    private static final long serialVersionUID     = -2914994073231782748L;

    public TooFewArgumentsException(Command command, int argumentCount, int[] allowedArgumentCount) {
        super(command);
        this.argumentCount = argumentCount;
        this.allowedArgumentCount = allowedArgumentCount;
    }

    public TooFewArgumentsException(Command command, String message, int argumentCount, int[] allowedArgumentCount) {
        super(command, message);
        this.argumentCount = argumentCount;
        this.allowedArgumentCount = allowedArgumentCount;
    }

    public int getArgumentCount() {
        return argumentCount;
    }

    public int[] getAllowedArgumentCount() {
        return allowedArgumentCount;
    }

    @Override
    public String getMessage() {
        return message == null ? "too few arguments - argument count: " + argumentCount + ", allowed: " + Arrays.toString(allowedArgumentCount) : message;
    }
}
