package mezzo.zz_bukkit.regionmanager.zz_old.exceptions.plugin;

/**
 * <code>MissingWorldGuardException</code>
 * 
 * @author mezzodrinker
 */
public class MissingWorldGuardException extends MissingPluginException {
    private static final long serialVersionUID = -669599189480694273L;

    public MissingWorldGuardException(String message) {
        super("WorldGuard", message);
    }

    public MissingWorldGuardException() {
        super("WorldGuard", null);
    }
}
