package mezzo.zz_bukkit.regionmanager.zz_old.exceptions.plugin;

/**
 * <code>MissingComponentException</code>
 * 
 * @author mezzodrinker
 */
public abstract class MissingComponentException extends Exception {
    private String            component        = null;
    private String            message          = null;

    private static final long serialVersionUID = -1909793310897465157L;

    protected MissingComponentException(String component, String message) {
        if (component == null) throw new IllegalArgumentException("component can not be null");
        this.component = component;
        this.message = message;
    }

    protected MissingComponentException(String component) {
        this(component, null);
    }

    @Override
    public String getMessage() {
        if (message == null) return "component " + component + " not found";
        return message;
    }

    public String getComponent() {
        return component;
    }
}
