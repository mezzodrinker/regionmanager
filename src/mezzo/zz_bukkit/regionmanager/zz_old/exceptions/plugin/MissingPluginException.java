package mezzo.zz_bukkit.regionmanager.zz_old.exceptions.plugin;

/**
 * <code>MissingPluginException</code>
 * 
 * @author mezzodrinker
 */
public abstract class MissingPluginException extends Exception {
    private String            plugin           = null;
    private String            message          = null;

    private static final long serialVersionUID = -9143819497162855100L;

    protected MissingPluginException(String plugin, String message) {
        if (plugin == null) throw new IllegalArgumentException("plugin can not be null");
        this.plugin = plugin;
        this.message = message;
    }

    protected MissingPluginException(String plugin) {
        this(plugin, null);
    }

    @Override
    public String getMessage() {
        if (message == null) return "plugin " + plugin + " not found";
        return message;
    }

    public String getPlugin() {
        return plugin;
    }
}
