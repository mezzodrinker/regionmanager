package mezzo.zz_bukkit.regionmanager.zz_old.exceptions.plugin.permission;

import mezzo.zz_bukkit.regionmanager.zz_old.exceptions.plugin.MissingComponentException;

/**
 * <code>MissingGroupSupportException</code>
 * 
 * @author mezzodrinker
 */
public class MissingGroupSupportException extends MissingComponentException {
    private static final long serialVersionUID = -1671750611639661758L;

    public MissingGroupSupportException(String component, String message) {
        super(component, message);
    }

    public MissingGroupSupportException(String message) {
        super("group support", message);
    }
}
