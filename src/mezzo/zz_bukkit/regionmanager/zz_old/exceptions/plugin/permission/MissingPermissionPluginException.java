package mezzo.zz_bukkit.regionmanager.zz_old.exceptions.plugin.permission;

import mezzo.zz_bukkit.regionmanager.zz_old.exceptions.plugin.MissingPluginException;

/**
 * <code>MissingPermissionPluginException</code>
 * 
 * @author mezzodrinker
 */
public class MissingPermissionPluginException extends MissingPluginException {
    private static final long serialVersionUID = 7819827938121167187L;

    public MissingPermissionPluginException(String plugin, String message) {
        super(plugin, message);
    }

    public MissingPermissionPluginException(String message) {
        super("permission plugin", message);
    }
}
