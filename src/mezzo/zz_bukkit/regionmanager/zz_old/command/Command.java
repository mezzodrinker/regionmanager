package mezzo.zz_bukkit.regionmanager.zz_old.command;

import static mezzo.util.ArrayUtil.array;
import static mezzo.zz_bukkit.regionmanager.zz_old.permission.PermissionManager.*;

import java.util.Arrays;

import mezzo.zz_bukkit.regionmanager.zz_old.exceptions.command.TooFewArgumentsException;
import mezzo.zz_bukkit.regionmanager.zz_old.exceptions.command.TooManyArgumentsException;
import mezzo.zz_bukkit.regionmanager.zz_old.message.Messages;

import org.bukkit.ChatColor;

/**
 * <code>Command</code>
 * 
 * @author mezzodrinker
 */
public enum Command {
    // /regionmanager
    // /regionmanager ?/help [topic]
    // /regionmanager config <option> [value]
    // /regionmanager config help [option]
    // /regionmanager save
    // /regionmanager reload [component]
    // /regionmanager region <region> <world>
    // /regionmanager region add <regex>
    // /regionmanager region delete <regex>
    // /regionmanager region members <region> <world>
    // /regionmanager region members add <region> <world> <username>
    // /regionmanager region members remove <region> <world> <username>
    // /regionmanager region owners <region> <world>
    // /regionmanager region owners add <region> <world> <username>
    // /regionmanager region owners remove <region> <world> <username>

    /**
     * {@code /regionmanager}<br>
     * Displays information about the current version of RegionManager.
     */
    REGIONMANAGER("regionmanager", array("rm", "region"), Messages.Command.Help.RegionManager.plugin, PERM_INFO, "", array(0), false, false, null),
    /**
     * {@code /regionmanager ?/help [topic]}<br>
     * Displays help on a given topic.
     */
    REGIONMANAGER_HELP("help", array("?"), Messages.Command.Help.RegionManager.help, PERM_COMMAND_HELP, "[topic]", array(0, 1), false, true, REGIONMANAGER),
    /**
     * {@code /regionmanager config <option> [value]}<br>
     * Writes or reads a config value.
     */
    REGIONMANAGER_CONFIG("config", Messages.Command.Help.RegionManager.config, PERM_COMMAND_CONFIG, "<option> [value]", array(1, 2), true, true, REGIONMANAGER),
    /**
     * {@code /regionmanager config help [option]}<br>
     * Displays help for config options.
     */
    REGIONMANAGER_CONFIG_HELP("help", Messages.Command.Help.RegionManager.config_help, PERM_COMMAND_CONFIG_HELP, "[option]", array(0, 1), true, true, REGIONMANAGER_CONFIG),
    /**
     * {@code /regionmanager save [component]}<br>
     * Saves all data of a given component.
     */
    REGIONMANAGER_SAVE("save", Messages.Command.Help.RegionManager.save, PERM_COMMAND_ADMIN_SAVE, "", array(0), true, false, REGIONMANAGER),
    /**
     * {@code /regionmanager reload [component]}<br>
     * Saves all data of a given component.
     */
    REGIONMANAGER_RELOAD("reload", Messages.Command.Help.RegionManager.reload, PERM_COMMAND_ADMIN_RELOAD, "[component]", array(0, 1), true, false, REGIONMANAGER),
    /**
     * TODO {@code /regionmanager region <region> <world>}<br>
     * Displays information on a given region.
     */
    REGIONMANAGER_REGION("region", Messages.Command.Help.RegionManager.region, PERM_COMMAND_REGION, "<region> <world>", "<region>", array(2), array(1), true, true, false, true, REGIONMANAGER),
    /**
     * TODO {@code /regionmanager region add <regex>}<br>
     * Adds all regions matching the given regex from all compatible region-providing plugins.
     */
    REGIONMANAGER_REGION_ADD("add", Messages.Command.Help.RegionManager.region_add, PERM_COMMAND_REGION_ADD, "<regex>", array(1), true, true, REGIONMANAGER_REGION),
    /**
     * TODO {@code /regionmanager region delete/remove <regex>}<br>
     * Deletes all regions matching the given regex.
     */
    REGIONMANAGER_REGION_DELETE("delete", array("remove", "del", "rem"), Messages.Command.Help.RegionManager.region_delete, PERM_COMMAND_REGION_DELETE, "<regex>", array(1), true, true,
            REGIONMANAGER_REGION),
    /**
     * TODO {@code /regionmanager region members <region>}<br>
     * Displays all members of the given region.
     */
    REGIONMANAGER_REGION_MEMBERS("members", Messages.Command.Help.RegionManager.region_members, PERM_COMMAND_REGION_MEMBERS, "<region> <world>", "<region>", array(2), array(1), true, true, false,
            true, REGIONMANAGER_REGION),
    /**
     * TODO {@code /regionmanager region members add <region> <username>} <br>
     * Sets the membership status of a player on the given region to member.
     */
    REGIONMANAGER_REGION_MEMBERS_ADD("add", Messages.Command.Help.RegionManager.region_members_add, PERM_COMMAND_REGION_MEMBERS_ADD, "<region> <world> <username>", "<region> <username>", array(3),
            array(2), true, true, false, true, REGIONMANAGER_REGION_MEMBERS),
    /**
     * TODO {@code /regionmanager region members remove <region> <username>} <br>
     * Revokes the membership status of a player on the given region.
     */
    REGIONMANAGER_REGION_MEMBERS_REMOVE("remove", Messages.Command.Help.RegionManager.region_members_remove, PERM_COMMAND_REGION_MEMBERS_REMOVE, "<region> <world> <username>", "<region> <username>",
            array(3), array(2), true, true, false, true, REGIONMANAGER_REGION_MEMBERS),
    /**
     * TODO {@code /regionmanager region owners <region>}<br>
     * Displays all owners of the given region.
     */
    REGIONMANAGER_REGION_OWNERS("owners", Messages.Command.Help.RegionManager.region_owners, PERM_COMMAND_REGION_OWNERS, "<region> <world>", "<region>", array(2), array(1), true, true, false, true,
            REGIONMANAGER_REGION),
    /**
     * TODO {@code /regionmanager region owners add <region> <username>}<br>
     * Sets the membership status of a player on the given region to owner.
     */
    REGIONMANAGER_REGION_OWNERS_ADD("add", Messages.Command.Help.RegionManager.region_owners_add, PERM_COMMAND_REGION_OWNERS_ADD, "<region> <world> <username>", "<region> <username>", array(3),
            array(2), true, true, false, true, REGIONMANAGER_REGION_OWNERS),
    /**
     * TODO {@code /regionmanager region owners remove <region> <username>}<br>
     * Revokes the membership status of a player on the given region.
     */
    REGIONMANAGER_REGION_OWNERS_REMOVE("remove", Messages.Command.Help.RegionManager.region_owners_remove, PERM_COMMAND_REGION_OWNERS_REMOVE, "<region> <world> <username>", "<region> <username>",
            array(3), array(2), true, true, false, true, REGIONMANAGER_REGION_OWNERS);

    private boolean   isPlayerCommand           = false;
    private boolean   isConsoleCommand          = false;
    private boolean   isAdminCommand            = false;
    private boolean   allowConcat               = false;
    private Integer[] argumentsAllowed          = null;
    private Integer[] argumentsAllowed_player   = null;
    private String    label                     = null;
    private String    permission                = null;
    private String    descriptionKey            = null;
    private String    syntax                    = null;
    private String    syntax_highlighted        = null;
    private String    syntax_player             = null;
    private String    syntax_player_highlighted = null;
    private String[]  alternateLabels           = new String[0];
    private Command   superCommand              = null;

    private Command(String label, String descriptionKey, String permission, String arguments, String arguments_player, Integer[] argumentsAllowed, Integer[] argumentsAllowed_player,
            boolean isPlayerCommand, boolean isConsoleCommand, boolean hideInCommandList, boolean allowConcat, Command superCommand) {
        // if (descriptionKey == null) throw new IllegalArgumentException("description key can not be null");
        // if (permission == null) throw new IllegalArgumentException("permission can not be null");
        // if (argumentsAllowed == null || argumentsAllowed.length == 0) throw new IllegalArgumentException("arguments allowed can not be null or empty");
        // if (argumentsAllowed_player == null || argumentsAllowed_player.length == 0) throw new IllegalArgumentException("arguments allowed (player) can not be null or empty");

        this.label = label;
        this.descriptionKey = descriptionKey;
        this.permission = permission;
        this.argumentsAllowed = argumentsAllowed;
        this.argumentsAllowed_player = argumentsAllowed_player;
        this.isPlayerCommand = isPlayerCommand;
        this.isConsoleCommand = isConsoleCommand;
        isAdminCommand = hideInCommandList;
        this.allowConcat = allowConcat;
        this.superCommand = superCommand;

        if (superCommand == null) {
            syntax = "/" + label;
            syntax_player = "/" + label;
        } else {
            syntax = "/" + superCommand.getLabel() + " " + label;
            syntax_player = "/" + superCommand.getLabel() + " " + label;
        }

        syntax_highlighted = ChatColor.YELLOW + syntax + ChatColor.WHITE;
        syntax_player_highlighted = ChatColor.YELLOW + syntax_player + ChatColor.WHITE;

        if (arguments != null && !arguments.isEmpty()) {
            syntax += " " + arguments;
            syntax_highlighted += " " + arguments;
        }
        if (arguments_player != null && !arguments_player.isEmpty()) {
            syntax_player += " " + arguments_player;
            syntax_player_highlighted += " " + arguments_player;
        }

        syntax_highlighted += ChatColor.RESET;
        syntax_player_highlighted += ChatColor.RESET;
    }

    private Command(String label, String[] alternateLabels, String descriptionKey, String permission, String arguments, String arguments_player, Integer[] argumentsAllowed,
            Integer[] argumentsAllowed_player, boolean isPlayerCommand, boolean isConsoleCommand, boolean isAdminCommand, boolean allowConcat, Command superCommand) {
        this(label, descriptionKey, permission, arguments, arguments_player, argumentsAllowed, argumentsAllowed_player, isPlayerCommand, isConsoleCommand, isAdminCommand, allowConcat, superCommand);
        this.alternateLabels = alternateLabels;
    }

    private Command(String label, String descriptionKey, String permission, String arguments, Integer[] argumentsAllowed, boolean isAdminCommand, boolean allowConcat, Command superCommand) {
        this(label, descriptionKey, permission, arguments, arguments, argumentsAllowed, argumentsAllowed, true, true, isAdminCommand, allowConcat, superCommand);
    }

    private Command(String label, String[] alternateLabels, String descriptionKey, String permission, String arguments, Integer[] argumentsAllowed, boolean isAdminCommand, boolean allowConcat,
            Command superCommand) {
        this(label, alternateLabels, descriptionKey, permission, arguments, arguments, argumentsAllowed, argumentsAllowed, true, true, isAdminCommand, allowConcat, superCommand);
    }

    public boolean isSubCommand() {
        return superCommand != null;
    }

    public boolean isPlayerCommand() {
        return isPlayerCommand;
    }

    public boolean isConsoleCommand() {
        return isConsoleCommand;
    }

    public boolean isAdminCommand() {
        return isAdminCommand;
    }

    public boolean allowsConcatenation() {
        return allowConcat;
    }

    public int[] getArgumentsAllowed(boolean isPlayer) {
        int[] ret = null;
        if (isPlayer) {
            ret = new int[argumentsAllowed_player.length];
            for (int i = 0; i < ret.length; i++) {
                ret[i] = argumentsAllowed_player[i];
            }
        } else {
            ret = new int[argumentsAllowed.length];
            for (int i = 0; i < ret.length; i++) {
                ret[i] = argumentsAllowed[i];
            }
        }
        return ret;
    }

    public boolean isArgumentCountAllowed(boolean isPlayer, int argumentCount) {
        if (isPlayer && !isPlayerCommand()) return false;
        if (!isPlayer && !isConsoleCommand()) return false;
        for (int i : getArgumentsAllowed(isPlayer)) {
            if (i == argumentCount) return true;
        }
        return false;
    }

    public void checkArgumentCount(boolean isPlayer, int argumentCount) throws TooManyArgumentsException, TooFewArgumentsException {
        if (isArgumentCountAllowed(isPlayer, argumentCount)) return;
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (int i : getArgumentsAllowed(isPlayer)) {
            min = Math.min(min, i);
            max = Math.max(max, i);
        }
        if (argumentCount < min) throw new TooFewArgumentsException(this, argumentCount, getArgumentsAllowed(isPlayer));
    }

    public String getLabel() {
        return label;
    }

    public String[] getAlternateLabels() {
        return alternateLabels;
    }

    public String getPermission() {
        return permission;
    }

    public String getDescriptionKey() {
        return descriptionKey;
    }

    public String getSyntax(boolean isPlayer) {
        if (isPlayer) return syntax_player;
        return syntax;
    }

    public String getSyntaxHighlighted(boolean isPlayer) {
        if (isPlayer) return syntax_player_highlighted;
        return syntax_highlighted;
    }

    public Command getSuperCommand() {
        return superCommand;
    }

    public Command[] getSubCommands() {
        Command[] ret = new Command[values().length];
        int i = 0;
        for (Command c : values()) {
            if (c.getSuperCommand() == null) {
                continue;
            }
            if (c.getSuperCommand().equals(this)) {
                ret[i++] = c;
            }
        }
        return Arrays.copyOf(ret, i);
    }

    public static Command getCommand(String label) {
        for (Command c : getCommands()) {
            if (c.getLabel().equalsIgnoreCase(label)) return c;
            for (String s : c.getAlternateLabels()) {
                if (s.equalsIgnoreCase(label)) return c;
            }
        }
        return null;
    }

    public static Command getCommand(String label, Command superCommand) {
        for (Command c : superCommand.getSubCommands()) {
            if (c.getLabel().equalsIgnoreCase(label)) return c;
            for (String s : c.getAlternateLabels()) {
                if (s.equalsIgnoreCase(label)) return c;
            }
        }
        return null;
    }

    public static Command[] getCommands() {
        Command[] ret = new Command[values().length];
        int i = 0;
        for (Command c : values()) {
            if (!c.isSubCommand()) {
                ret[i++] = c;
            }
        }
        return Arrays.copyOf(ret, i);
    }
}