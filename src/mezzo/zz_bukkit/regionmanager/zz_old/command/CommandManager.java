package mezzo.zz_bukkit.regionmanager.zz_old.command;

import static mezzo.zz_bukkit.regionmanager.zz_old.command.Command.*;
import static mezzo.zz_bukkit.regionmanager.zz_old.message.MessageManager.replacement;
import static mezzo.zz_bukkit.regionmanager.zz_old.message.MessageManager.sendMessage;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mezzo.bukkit.command.ICommandManager;
import mezzo.util.configuration.ConfigurationSection;
import mezzo.util.math.Vector3;
import mezzo.util.wrappers.SerializableColor;
import mezzo.zz_bukkit.regionmanager.zz_old.RegionManagerPlugin;
import mezzo.zz_bukkit.regionmanager.zz_old.configuration.ConfigManager;
import mezzo.zz_bukkit.regionmanager.zz_old.configuration.util.ConfigOption;
import mezzo.zz_bukkit.regionmanager.zz_old.configuration.util.ConfigOption.DataType;
import mezzo.zz_bukkit.regionmanager.zz_old.exceptions.command.ArgumentConversionException;
import mezzo.zz_bukkit.regionmanager.zz_old.exceptions.command.CommandException;
import mezzo.zz_bukkit.regionmanager.zz_old.exceptions.command.TooFewArgumentsException;
import mezzo.zz_bukkit.regionmanager.zz_old.exceptions.command.TooManyArgumentsException;
import mezzo.zz_bukkit.regionmanager.zz_old.exceptions.command.UnimplementedCommandException;
import mezzo.zz_bukkit.regionmanager.zz_old.exceptions.world.WorldDisabledException;
import mezzo.zz_bukkit.regionmanager.zz_old.message.Messages;
import mezzo.zz_bukkit.regionmanager.zz_old.message.Variables;
import mezzo.zz_bukkit.regionmanager.zz_old.region.Region;
import mezzo.zz_bukkit.regionmanager.zz_old.region.RegionManager;
import mezzo.zz_bukkit.regionmanager.zz_old.util.Player;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.inventory.ItemStack;

/**
 * <code>CommandManager</code>
 * 
 * @author mezzodrinker
 */
public class CommandManager implements ICommandManager {
    private RegionManagerPlugin plugin          = null;
    private CommandExecutor     commandExecutor = null;

    public CommandManager(RegionManagerPlugin plugin) {
        this.plugin = plugin;
        commandExecutor = new CommandExecutor(plugin);
    }

    private Object argumentConversion(String valueIn, DataType dataType) throws ArgumentConversionException {
        Object value = null;
        final String regexFloat = "(\\d+([\\.,]\\d+)?)";
        Map<String, Boolean> bmap = new HashMap<String, Boolean>();
        String regex;
        NumberFormat format = NumberFormat.getInstance();
        bmap.put("true", true);
        bmap.put("false", false);
        bmap.put("yes", true);
        bmap.put("no", false);
        bmap.put("allow", true);
        bmap.put("deny", false);
        switch (dataType) {
            case BOOLEAN:
                if (bmap.get(valueIn.toLowerCase()) != null) {
                    value = bmap.get(valueIn.toLowerCase());
                } else
                    throw new ArgumentConversionException(REGIONMANAGER_CONFIG, valueIn, String.class, boolean.class);
                break;
            case COLOR:
                if (valueIn.matches("\\(\\d+,\\d+,\\d+(,\\d+)?\\)")) {
                    valueIn = valueIn.replace("(", "");
                    valueIn = valueIn.replace(")", "");
                    String[] s = valueIn.split(",");
                    int r = Integer.valueOf(s[0]);
                    int g = Integer.valueOf(s[1]);
                    int b = Integer.valueOf(s[2]);
                    int a = 255;
                    if (s.length > 3) {
                        a = Integer.valueOf(s[3]);
                    }
                    value = new SerializableColor(r, g, b, a);
                } else if (valueIn.matches("[0-9a-fA-F]{6}|[0-9a-fA-F]{8}")) {
                    int rgba = Integer.valueOf(valueIn, 16);
                    value = new SerializableColor(rgba, valueIn.length() == 8);
                } else
                    throw new ArgumentConversionException(REGIONMANAGER_CONFIG, valueIn, String.class, SerializableColor.class);
                break;
            case DOUBLE:
            case INT:
            case LONG:
                Number n;
                try {
                    n = format.parse(valueIn);
                } catch (ParseException e) {
                    throw new ArgumentConversionException(REGIONMANAGER_CONFIG, valueIn, String.class, Number.class, e);
                }
                switch (dataType) {
                    case DOUBLE:
                        value = n.doubleValue();
                        break;
                    case INT:
                        value = n.intValue();
                        break;
                    case LONG:
                        value = n.longValue();
                        break;
                    default:
                }
                break;
            case ITEMSTACK:
                throw new ArgumentConversionException(REGIONMANAGER_CONFIG, String.class, ItemStack.class);
            case LIST:
                throw new ArgumentConversionException(REGIONMANAGER_CONFIG, String.class, List.class);
            case CHARACTER_LIST:
                char[] array = valueIn.toCharArray();
                List<Character> clist = new ArrayList<Character>();
                for (char c : array) {
                    clist.add(c);
                }
                value = clist;
                break;
            case BOOLEAN_LIST:
                regex = ".+(,.+)*";
                if (valueIn.matches("\\[" + regex + "\\]")) {
                    valueIn = valueIn.replace("]", "");
                    valueIn = valueIn.replace("[", "");
                }
                if (valueIn.matches(regex)) {
                    List<Boolean> boolist = new ArrayList<Boolean>();
                    String[] s = valueIn.split(",");
                    for (String str : s) {
                        if (bmap.get(str) != null) {
                            boolist.add(bmap.get(str));
                        } else
                            throw new ArgumentConversionException(REGIONMANAGER_CONFIG, valueIn, String.class, boolean.class);
                    }
                    value = boolist;
                } else
                    throw new ArgumentConversionException(REGIONMANAGER_CONFIG, valueIn, String.class, List.class);
                break;
            case BYTE_LIST:
            case DOUBLE_LIST:
            case FLOAT_LIST:
            case INTEGER_LIST:
            case LONG_LIST:
            case SHORT_LIST:
                regex = regexFloat + "(;" + regexFloat + ")*";
                if (valueIn.matches("\\[" + regex + "\\]")) {
                    valueIn = valueIn.replace("[", "");
                    valueIn = valueIn.replace("]", "");
                }
                if (valueIn.matches(regex)) {
                    List<Number> l = new ArrayList<Number>();
                    String[] s = valueIn.split(";");
                    for (String str : s) {
                        try {
                            l.add(format.parse(str));
                        } catch (ParseException e) {
                            throw new ArgumentConversionException(REGIONMANAGER_CONFIG, str, String.class, Number.class, e);
                        }
                    }
                    switch (dataType) {
                        case BYTE_LIST:
                            List<Byte> blist = new ArrayList<Byte>();
                            for (Number number : l) {
                                blist.add(number.byteValue());
                            }
                            value = blist;
                            break;
                        case DOUBLE_LIST:
                            List<Double> dlist = new ArrayList<Double>();
                            for (Number number : l) {
                                dlist.add(number.doubleValue());
                            }
                            value = dlist;
                            break;
                        case FLOAT_LIST:
                            List<Float> flist = new ArrayList<Float>();
                            for (Number number : l) {
                                flist.add(number.floatValue());
                            }
                            value = flist;
                            break;
                        case INTEGER_LIST:
                            List<Integer> ilist = new ArrayList<Integer>();
                            for (Number number : l) {
                                ilist.add(number.intValue());
                            }
                            value = ilist;
                            break;
                        case LONG_LIST:
                            List<Long> llist = new ArrayList<Long>();
                            for (Number number : l) {
                                llist.add(number.longValue());
                            }
                            value = llist;
                            break;
                        case SHORT_LIST:
                            List<Short> slist = new ArrayList<Short>();
                            for (Number number : l) {
                                slist.add(number.shortValue());
                            }
                            value = slist;
                            break;
                        default:
                    }
                } else
                    throw new ArgumentConversionException(REGIONMANAGER_CONFIG, valueIn, String.class, List.class);
                break;
            case STRING_LIST:
                regex = ".+(;.+)*";
                if (valueIn.matches("\\[" + regex + "\\]")) {
                    valueIn = valueIn.replace("[", "");
                    valueIn = valueIn.replace("]", "");
                }
                if (valueIn.matches(regex)) {
                    List<String> l = new ArrayList<String>();
                    String[] s = valueIn.split(";");
                    for (String str : s) {
                        l.add(str);
                    }
                    value = l;
                } else
                    throw new ArgumentConversionException(REGIONMANAGER_CONFIG, valueIn, String.class, List.class);
                break;
            case MAP_LIST:
                throw new ArgumentConversionException(REGIONMANAGER_CONFIG, String.class, List.class);
            case OBJECT:
                value = valueIn;
                break;
            case OFFLINEPLAYER:
                value = Bukkit.getOfflinePlayer(valueIn);
                break;
            case STRING:
                value = valueIn;
                break;
            case VECTOR:
                if (valueIn.matches("\\(" + regexFloat + "(\\|" + regexFloat + "(\\|" + regexFloat + ")?)?\\)")) {
                    valueIn = valueIn.replace("(", "");
                    valueIn = valueIn.replace(")", "");
                    String[] s = valueIn.split("\\|");
                    int x = Integer.valueOf(s[0]);
                    int y = 0;
                    int z = 0;
                    if (s.length > 1) {
                        y = Integer.valueOf(s[1]);
                    }
                    if (s.length > 2) {
                        z = Integer.valueOf(s[2]);
                    }
                    value = new Vector3(x, y, z);
                } else
                    throw new ArgumentConversionException(REGIONMANAGER_CONFIG, valueIn, String.class, Vector3.class);
                break;
            case CONFIGURATIONSECTION:
                throw new ArgumentConversionException(REGIONMANAGER_CONFIG, String.class, ConfigurationSection.class);
        }
        return value;
    }

    public void regionmanager(CommandSender sender, String[] arguments) {
        commandExecutor.pluginInfo(sender);
    }

    public void regionmanagerHelp(CommandSender sender, String[] arguments) {
        if (arguments.length > 1 && REGIONMANAGER_HELP.allowsConcatenation()) {
            arguments = trimArguments(arguments, 1);
        }

        if (arguments.length == 0) { // /regionmanager ?/help
            commandExecutor.help(sender);
        } else if (arguments.length == 1) { // /regionmanager ?/help <topic>
            commandExecutor.help(sender, arguments[0]);
        }
    }

    public void regionmanagerConfig(CommandSender sender, String[] arguments) throws CommandException {
        if (arguments.length > 2 && REGIONMANAGER_CONFIG.allowsConcatenation()) {
            arguments = trimArguments(arguments, 2);
        }

        ConfigOption c = null;
        if (arguments.length == 0) {
            StringBuilder configOptions = new StringBuilder();
            for (ConfigOption option : ConfigManager.CONFIG_MAIN_OPTIONS.values()) {
                String key = option.getInGameKey();
                if (key == null) {
                    continue;
                }
                String type = plugin.configManager().getType(option.getKey());
                if (configOptions.length() > 0) {
                    configOptions.append(", ");
                }
                configOptions.append(key).append(": ").append(type);
            }
            plugin.messageManager().messageWrapped(Messages.Configuration.availableKeys, replacement(Variables.options, configOptions));
            return;
        }
        c = plugin.configManager().getInGameConfigOption(arguments[0]);

        if (arguments.length == 1) { // /regionmanager config <option>
            commandExecutor.configGet(sender, c.getKey());
        } else if (arguments.length == 2) { // /regionmanager config <option> <value>
            commandExecutor.configSet(sender, c.getKey(), argumentConversion(arguments[1], c.getDataType()));
        }
    }

    public void regionmanagerConfigHelp(CommandSender sender, String[] arguments) {
        if (arguments.length > 1 && REGIONMANAGER_CONFIG_HELP.allowsConcatenation()) {
            arguments = trimArguments(arguments, 1);
        }

        if (arguments.length == 0) { // /regionmanager config help
            commandExecutor.configHelp(sender);
        } else if (arguments.length == 1) { // /regionmanager config help <option>
            commandExecutor.configHelp(sender, arguments[0]);
        }
    }

    public void regionmanagerReload(CommandSender sender, String[] arguments) {
        if (arguments.length > 1 && REGIONMANAGER_RELOAD.allowsConcatenation()) {
            arguments = trimArguments(arguments, 1);
        }

        if (arguments.length == 0) { // /regionmanager reload
            commandExecutor.reload(sender);
        } else if (arguments.length == 1) { // /regionmanager reload <component>
            commandExecutor.reload(sender, arguments[0]);
        }
    }

    public void regionmanagerSave(CommandSender sender, String[] arguments) {
        if (arguments.length > 1 && REGIONMANAGER_SAVE.allowsConcatenation()) {
            arguments = trimArguments(arguments, 1);
        }

        if (arguments.length == 0) { // /regionmanager save
            commandExecutor.save(sender);
        } else if (arguments.length == 1) { // /regionmanager save <component>
            commandExecutor.save(sender, arguments[0]);
        }
    }

    public void regionmanagerRegion(CommandSender sender, String[] arguments) {
        // TODO CommandManager.regionmanagerRegion(sender, arguments)
        if (sender instanceof org.bukkit.entity.Player) {
            if (arguments.length > 1 && REGIONMANAGER_REGION.allowsConcatenation()) {
                arguments = trimArguments(arguments, 1);
            }

            // commandExecutor.regionInfo(sender, arguments[0], ((org.bukkit.entity.Player) sender).getWorld());
        } else if (sender instanceof ConsoleCommandSender) {
            if (arguments.length > 2 && REGIONMANAGER_REGION.allowsConcatenation()) {
                arguments = trimArguments(arguments, 2);
            }

            // commandExecutor.regionInfo(sender, arguments[0], plugin.getServer().getWorld(arguments[1]));
        }
    }

    public void regionmanagerRegionAdd(CommandSender sender, String[] arguments) {
        // TODO CommandManager.regionmanagerRegionAdd(sender, arguments)
        if (arguments.length > 1 && REGIONMANAGER_REGION_ADD.allowsConcatenation()) {
            arguments = trimArguments(arguments, 1);
        }

        // commandExecutor.regionAdd(sender, arguments[0]);
    }

    public void regionmanagerRegionDelete(CommandSender sender, String[] arguments) {
        // TODO CommandManager.regionmanagerRegionRemove(sender, arguments)
        if (arguments.length > 1 && REGIONMANAGER_REGION_DELETE.allowsConcatenation()) {
            arguments = trimArguments(arguments, 1);
        }

        // commandExecutor.regionDelete(sender, arguments[0]);
    }

    public void regionmanagerRegionMembers(CommandSender sender, String[] arguments) {
        // TODO CommandManager.regionmanagerRegionMembers(sender, arguments)
        World world = null;
        String regionname = null;
        if (sender instanceof org.bukkit.entity.Player) {
            if (arguments.length > 1 && REGIONMANAGER_REGION_MEMBERS.allowsConcatenation()) {
                arguments = trimArguments(arguments, 1);
            }

            world = ((org.bukkit.entity.Player) sender).getWorld();
            regionname = arguments[0];
        } else {
            if (arguments.length > 2 && REGIONMANAGER_REGION_MEMBERS.allowsConcatenation()) {
                arguments = trimArguments(arguments, 2);
            }

            regionname = arguments[0];
            world = plugin.getServer().getWorld(arguments[1]);
            if (world == null) {
                sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Command.Error.World.invalid, replacement(Variables.world, arguments[1])));
                return;
            }
        }
        RegionManager regionmanager;
        try {
            regionmanager = plugin.worldManager().regionManager(world);
        } catch (WorldDisabledException e) {
            sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Command.Error.World.disabled, replacement(Variables.world, world.getName())));
            return;
        }
        Region region = regionmanager.getRegion(regionname);
        if (region == null) {
            sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Command.Error.Region.invalid, replacement(Variables.region, regionname)));
            return;
        }
        // commandExecutor.regionMembers(sender, world, region);
    }

    public void regionmanagerRegionMembersAdd(CommandSender sender, String[] arguments) {
        // TODO CommandManager.regionmanagerRegionMembersAdd(sender, arguments)
        World world = null;
        String regionname = null;
        Player target = null;
        if (sender instanceof org.bukkit.entity.Player) {
            if (arguments.length > 2 && REGIONMANAGER_REGION_MEMBERS_ADD.allowsConcatenation()) {
                arguments = trimArguments(arguments, 1);
            }

            world = ((org.bukkit.entity.Player) sender).getWorld();
            regionname = arguments[0];
            target = Player.valueOf(plugin.getServer().getOfflinePlayer(arguments[1]), plugin);
        }
    }

    public void regionmanagerRegionMembersRemove(CommandSender sender, String[] arguments) {
        // TODO CommandManager.regionmanagerRegionMembersRemove(sender, arguments)
    }

    public void regionmanagerRegionOwners(CommandSender sender, String[] arguments) {
        // TODO CommandManager.regionmanagerRegionOwners(sender, arguments)
    }

    public void regionmanagerRegionOwnersAdd(CommandSender sender, String[] arguments) {
        // TODO CommandManager.regionmanagerRegionOwnersAdd(sender, arguments)
    }

    public void regionmanagerRegionOwnersRemove(CommandSender sender, String[] arguments) {
        // TODO CommandManager.regionmanagerRegionOwnersRemove(sender, arguments)
    }

    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command c, String label, String[] arguments) {
        if (c.getAliases().contains("regionmanager")) {
            if (arguments == null) {
                arguments = new String[0];
            }

            Command command = REGIONMANAGER;
            Command[] subCommands;
            while ((subCommands = command.getSubCommands()) != null && subCommands.length > 0 && arguments.length > 0) {
                Command buf = Command.getCommand(arguments[0], command);
                if (buf == null) {
                    break;
                }
                command = buf;
                arguments = Arrays.copyOfRange(arguments, 1, arguments.length);
            }

            if (command == null) {
                sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Command.Error.notRecognized, replacement(Variables.command, label + (arguments.length > 0 ? arguments[0] : ""))));
                if (plugin.permissionManager().canExecute(sender, REGIONMANAGER_HELP)) {
                    command = REGIONMANAGER_HELP;
                } else
                    return true;
            }

            // permission check
            if (!plugin.permissionManager().canExecute(sender, command)) {
                if (command.isAdminCommand()) {
                    sendMessage(
                            sender,
                            plugin.messageManager().messageWrapped(Messages.Command.Error.insufficientPermissions_hidden,
                                    replacement(Variables.command, command.getSyntax(sender instanceof org.bukkit.entity.Player))));
                    return true;
                }
                sendMessage(
                        sender,
                        plugin.messageManager().messageWrapped(Messages.Command.Error.insufficientPermissions,
                                replacement(Variables.command, command.getSyntax(sender instanceof org.bukkit.entity.Player)), replacement(Variables.syntax, command.getPermission())));
                return true;
            }

            try {
                command.checkArgumentCount(sender instanceof org.bukkit.entity.Player, arguments.length);
            } catch (TooManyArgumentsException e) {
                if (command.allowsConcatenation()) {
                    int length = Integer.MIN_VALUE;
                    for (int i : e.getAllowedArgumentCount()) {
                        length = Math.max(length, i);
                    }
                    arguments = trimArguments(arguments, length);
                } else {
                    sendMessage(
                            sender,
                            plugin.messageManager().messageWrapped(Messages.Command.Error.tooManyArguments,
                                    replacement(Variables.syntax, command.getSyntax(sender instanceof org.bukkit.entity.Player)), replacement(Variables.argumentCount, e.getArgumentCount()),
                                    replacement(Variables.argumentCount_correct, Arrays.toString(e.getAllowedArgumentCount()))));
                }
                return true;
            } catch (TooFewArgumentsException e) {
                sendMessage(
                        sender,
                        plugin.messageManager().messageWrapped(Messages.Command.Error.tooFewArguments, replacement(Variables.syntax, command.getSyntax(sender instanceof org.bukkit.entity.Player)),
                                replacement(Variables.argumentCount, e.getArgumentCount()), replacement(Variables.argumentCount_correct, Arrays.toString(e.getAllowedArgumentCount()))));
            }

            try {
                switch (command) {
                    case REGIONMANAGER:
                        regionmanager(sender, arguments);
                        break;
                    case REGIONMANAGER_CONFIG:
                        regionmanagerConfig(sender, arguments);
                        break;
                    case REGIONMANAGER_CONFIG_HELP:
                        regionmanagerConfigHelp(sender, arguments);
                        break;
                    case REGIONMANAGER_HELP:
                        regionmanagerHelp(sender, arguments);
                        break;
                    case REGIONMANAGER_RELOAD:
                        regionmanagerReload(sender, arguments);
                        break;
                    case REGIONMANAGER_SAVE:
                        regionmanagerSave(sender, arguments);
                        break;
                    case REGIONMANAGER_REGION:
                        regionmanagerRegion(sender, arguments);
                        break;
                    case REGIONMANAGER_REGION_ADD:
                        regionmanagerRegionAdd(sender, arguments);
                        break;
                    case REGIONMANAGER_REGION_DELETE:
                        regionmanagerRegionDelete(sender, arguments);
                        break;
                    case REGIONMANAGER_REGION_MEMBERS:
                        regionmanagerRegionMembers(sender, arguments);
                        break;
                    case REGIONMANAGER_REGION_MEMBERS_ADD:
                        regionmanagerRegionMembersAdd(sender, arguments);
                        break;
                    case REGIONMANAGER_REGION_MEMBERS_REMOVE:
                        regionmanagerRegionMembersRemove(sender, arguments);
                        break;
                    case REGIONMANAGER_REGION_OWNERS:
                        regionmanagerRegionOwners(sender, arguments);
                        break;
                    case REGIONMANAGER_REGION_OWNERS_ADD:
                        regionmanagerRegionOwnersAdd(sender, arguments);
                        break;
                    case REGIONMANAGER_REGION_OWNERS_REMOVE:
                        regionmanagerRegionOwnersRemove(sender, arguments);
                        break;
                    default:
                        throw new UnimplementedCommandException(command);
                }
            } catch (Throwable t) {
                sendMessage(
                        sender,
                        plugin.messageManager().messageWrapped(Messages.Command.Error.exception, replacement(Variables.errorType, t.getClass().getSimpleName()),
                                replacement(Variables.errorMessage, t.getMessage())));
            }

            return true;
        }
        return false;
    }

    private static String[] trimArguments(String[] args, int length) {
        String[] ret = new String[args.length];
        for (int i = 0; i < args.length; i++) {
            if (i < length - 1) {
                ret[i] = args[i];
            } else {
                if (ret[length - 1] == null) {
                    ret[length - 1] = args[i];
                } else {
                    ret[length - 1] += " " + args[i];
                }
            }
        }
        return Arrays.copyOf(ret, length);
    }
}
