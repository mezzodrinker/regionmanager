package mezzo.zz_bukkit.regionmanager.zz_old.command;

import static mezzo.zz_bukkit.regionmanager.zz_old.message.MessageManager.HEADING;
import static mezzo.zz_bukkit.regionmanager.zz_old.message.MessageManager.replacement;
import static mezzo.zz_bukkit.regionmanager.zz_old.message.MessageManager.sendMessage;

import java.io.IOException;
import java.util.List;

import mezzo.bukkit.net.Updater.UpdateResult;
import mezzo.zz_bukkit.regionmanager.zz_old.Component;
import mezzo.zz_bukkit.regionmanager.zz_old.RegionManagerPlugin;
import mezzo.zz_bukkit.regionmanager.zz_old.configuration.ConfigManager;
import mezzo.zz_bukkit.regionmanager.zz_old.message.Messages;
import mezzo.zz_bukkit.regionmanager.zz_old.message.Variables;
import mezzo.zz_bukkit.regionmanager.zz_old.message.MessageManager.ConfigHelp;

import org.bukkit.command.CommandSender;

/**
 * <code>CommandExecutor</code>
 * 
 * @author mezzodrinker
 */
public class CommandExecutor {
    private RegionManagerPlugin plugin = null;

    public CommandExecutor(RegionManagerPlugin plugin) {
        this.plugin = plugin;
    }

    public void pluginInfo(CommandSender sender) {
        // create a list of authors
        StringBuilder authors = new StringBuilder();
        List<String> l = plugin.getDescription().getAuthors();
        for (int i = 0; i < l.size(); i++) {
            String author = l.get(i);
            if (i == 0) {
                authors.append(author);
            } else if (i == l.size() - 1) {
                authors.append(plugin.messageManager().messageWrapped(Messages.Language.concatenation)).append(" ").append(author);
            } else {
                authors.append(", ").append(author);
            }
        }

        // get the most recent update result; if none is available, check for updates
        UpdateResult update = plugin.updateChecker().getResult();
        if (update == null) {
            update = plugin.updateChecker().check();
        }
        String updateAvailable = update == UpdateResult.UPDATE_AVAILABLE ? plugin.messageManager().messageWrapped(Messages.Language.yes) : plugin.messageManager().messageWrapped(Messages.Language.no);

        // build the message
        StringBuilder message = new StringBuilder();
        message.append(HEADING);
        message.append(plugin.messageManager().messageWrapped(Messages.PluginInfo.description) + "\n");
        message.append("\n");
        message.append(plugin.messageManager().messageWrapped(Messages.PluginInfo.authors, replacement(Variables.authors, authors.toString())) + "\n");
        message.append(plugin.messageManager().messageWrapped(Messages.PluginInfo.website, replacement(Variables.website, plugin.getDescription().getWebsite())) + "\n");
        message.append(plugin.messageManager().messageWrapped(Messages.PluginInfo.version, replacement(Variables.version, plugin.getDescription().getVersion())) + "\n");
        message.append(plugin.messageManager().messageWrapped(Messages.PluginInfo.updateAvailable, replacement(Variables.yesNo, updateAvailable)) + "\n");
        message.append("\n");
        message.append(plugin.messageManager().messageWrapped(Messages.PluginInfo.helpLine, replacement(Variables.command, Command.REGIONMANAGER_HELP.getSyntax(true))));
        message.append("\n");

        // send the message to the command issuer
        sendMessage(sender, message.toString());
    }

    // command help
    public void help(CommandSender sender) {
        help(sender, "1");
    }

    public void help(CommandSender sender, String topic) {
        int page = -1;
        try {
            page = Integer.valueOf(topic);
        } catch (NumberFormatException e) {
        }

        if (page > 0) {
            // build the message
            StringBuilder message = new StringBuilder();
            Command c = Command.values()[0];

            message.append(plugin.messageManager().messageWrapped(Messages.Command.Help.heading, replacement(Variables.page, page)));
            message.append(plugin.messageManager().messageWrapped(Messages.Command.Help.note));
            for (int i = (page - 1) * 10; i < page * 10 && i < Command.values().length; c = Command.values()[++i]) {
                if (!plugin.permissionManager().canExecute(sender, c)) {
                    continue;
                }
                message.append(c.getSyntaxHighlighted(sender instanceof org.bukkit.entity.Player));
                message.append(" - ");
                message.append(plugin.messageManager().messageWrapped(c.getDescriptionKey()));
                message.append("\n");
            }
            message.append("\n");
            message.append(plugin.messageManager().messageWrapped(Messages.Command.Help.nextPage, replacement(Variables.page, page + 1))).append("\n");
            message.append("\n");

            // send the message to the command issuer
            sendMessage(sender, message.toString());
            return;
        }

        String message = plugin.messageManager().help().getString(topic);
        if (message == null) {
            // the entered topic was not found
            sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Help.topicNotFound, replacement(Variables.topic, topic)));
        }
        return;
    }

    public void configSet(CommandSender sender, String key, Object value) {
        int configID = plugin.configManager().getConfigIDFor(key);
        switch (configID) {
            case ConfigManager.MAIN_CONFIG:
                plugin.configManager().config().set(key, value);
                sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Configuration.write, replacement(Variables.option, key), replacement(Variables.value, value)));
                break;
            case ConfigManager.REGIONS_CONFIG:
                plugin.configManager().regionsConfig().set(key, value);
                sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Configuration.write, replacement(Variables.option, key), replacement(Variables.value, value)));
                break;
            case ConfigManager.PLAYER_CONFIG:
                sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Configuration.PlayerConfiguration.write));
                break;
            default:
                sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Configuration.error, replacement(Variables.option, key)));
        }
    }

    public void configGet(CommandSender sender, String key) {
        int configID = plugin.configManager().getConfigIDFor(key);
        Object value = null;

        switch (configID) {
            case ConfigManager.MAIN_CONFIG:
                value = plugin.configManager().config().get(key);
                break;
            case ConfigManager.REGIONS_CONFIG:
                value = plugin.configManager().regionsConfig().get(key);
            case ConfigManager.PLAYER_CONFIG:
                sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Configuration.PlayerConfiguration.read));
                return;
            default:
                sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Configuration.error, replacement(Variables.option, key)));
                return;
        }

        sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Configuration.read, replacement(Variables.option, key), replacement(Variables.value, value)));
    }

    public void configHelp(CommandSender sender) {
        sendMessage(sender, plugin.messageManager().configHelp().getString(ConfigHelp.general));
    }

    public void configHelp(CommandSender sender, String topic) {
        String help = plugin.messageManager().configHelp().getString(topic);
        if (help == null) {
            configHelp(sender);
            return;
        }
        sendMessage(sender, help);
    }

    public void reload(CommandSender sender) {
        sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Reload.started));
        try {
            plugin.load();
        } catch (IOException e) {
            sendMessage(
                    sender,
                    plugin.messageManager().messageWrapped(Messages.Reload.failed, replacement(Variables.errorType, e.getClass().getSimpleName()),
                            replacement(Variables.errorMessage, e.getLocalizedMessage())));
            return;
        }
        sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Reload.succeeded));
    }

    public void reload(CommandSender sender, String component) {
        if (component.startsWith("#") && component.endsWith("#")) {
            // use regex
            String componentRegex = component.substring(1, component.length() - 2);
            List<Component> components = plugin.getComponents(componentRegex);
            if (components.isEmpty()) {
                // no applicable component was found
                sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Component.notFound_regex, replacement(Variables.component, componentRegex)));
                return;
            }
            for (Component c : components) {
                sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Component.Reload.started, replacement(Variables.component, c.getID())));
                try {
                    c.load();
                } catch (IOException e) {
                    sendMessage(
                            sender,
                            plugin.messageManager().messageWrapped(Messages.Component.Reload.failed, replacement(Variables.component, c.getID()),
                                    replacement(Variables.errorType, e.getClass().getSimpleName()), replacement(Variables.errorMessage, e.getLocalizedMessage())));
                    continue;
                }
                sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Component.Reload.succeeded, replacement(Variables.component, c.getID())));
            }
        } else {
            // raw ID
            Component c = plugin.getComponent(component);
            if (c == null) {
                // no applicable component was found
                sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Component.notFound, replacement(Variables.component, component)));
                return;
            }
            sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Component.Reload.started, replacement(Variables.component, c.getID())));
            try {
                c.load();
            } catch (IOException e) {
                sendMessage(
                        sender,
                        plugin.messageManager().messageWrapped(Messages.Component.Reload.failed, replacement(Variables.component, c.getID()),
                                replacement(Variables.errorType, e.getClass().getSimpleName()), replacement(Variables.errorMessage, e.getLocalizedMessage())));
                return;
            }
            sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Component.Reload.succeeded, replacement(Variables.component, c.getID())));
        }
    }

    public void save(CommandSender sender) {
        sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Save.started));
        try {
            plugin.save();
        } catch (IOException e) {
            sendMessage(
                    sender,
                    plugin.messageManager().messageWrapped(Messages.Save.failed, replacement(Variables.errorType, e.getClass().getSimpleName()),
                            replacement(Variables.errorMessage, e.getLocalizedMessage())));
            return;
        }
        sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Save.succeeded));
    }

    public void save(CommandSender sender, String component) {
        if (component.startsWith("#") && component.endsWith("#")) {
            // use regex
            String componentRegex = component.substring(1, component.length() - 2);
            List<Component> components = plugin.getComponents(componentRegex);
            if (components.isEmpty()) {
                // no applicable component was found
                sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Component.notFound_regex, replacement(Variables.component, componentRegex)));
                return;
            }
            for (Component c : components) {
                sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Component.Save.started, replacement(Variables.component, c.getID())));
                try {
                    c.save();
                } catch (IOException e) {
                    sendMessage(
                            sender,
                            plugin.messageManager().messageWrapped(Messages.Component.Save.failed, replacement(Variables.component, c.getID()),
                                    replacement(Variables.errorType, e.getClass().getSimpleName()), replacement(Variables.errorMessage, e.getLocalizedMessage())));
                    continue;
                }
                sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Component.Save.succeeded, replacement(Variables.component, c.getID())));
            }
        } else {
            // raw ID
            Component c = plugin.getComponent(component);
            if (c == null) {
                // no applicable component was found
                sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Component.notFound, replacement(Variables.component, component)));
                return;
            }
            sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Component.Save.started, replacement(Variables.component, c.getID())));
            try {
                c.save();
            } catch (IOException e) {
                sendMessage(
                        sender,
                        plugin.messageManager().messageWrapped(Messages.Component.Save.failed, replacement(Variables.component, c.getID()),
                                replacement(Variables.errorType, e.getClass().getSimpleName()), replacement(Variables.errorMessage, e.getLocalizedMessage())));
                return;
            }
            sendMessage(sender, plugin.messageManager().messageWrapped(Messages.Component.Save.succeeded, replacement(Variables.component, c.getID())));
        }
    }
}
