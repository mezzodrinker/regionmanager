package mezzo.zz_bukkit.regionmanager.zz_old.compatibility;

import java.io.IOException;

import mezzo.zz_bukkit.regionmanager.zz_old.Component;
import mezzo.zz_bukkit.regionmanager.zz_old.RegionManagerPlugin;
import mezzo.zz_bukkit.regionmanager.zz_old.economy.DefaultEconomyProvider;
import mezzo.zz_bukkit.regionmanager.zz_old.economy.EconomyProvider;
import mezzo.zz_bukkit.regionmanager.zz_old.economy.VaultEconomyProvider;
import mezzo.zz_bukkit.regionmanager.zz_old.permission.BukkitPermissionProvider;
import mezzo.zz_bukkit.regionmanager.zz_old.permission.PermissionProvider;
import mezzo.zz_bukkit.regionmanager.zz_old.permission.VaultPermissionProvider;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

/**
 * <code>CompatManager</code>
 * 
 * @author mezzodrinker
 */
public class CompatManager implements Component {
    private RegionManagerPlugin plugin             = null;
    private WorldGuardPlugin    worldGuard         = null;
    private EconomyProvider     economyProvider    = null;
    private PermissionProvider  permissionProvider = null;

    private static final String WORLDGUARD_ID      = "WorldGuard";

    public CompatManager(RegionManagerPlugin plugin) {
        this.plugin = plugin;

        load();
    }

    private void loadWorldGuard() {
        plugin.logger().log("loading WorldGuard");
        Plugin p = plugin.getServer().getPluginManager().getPlugin(WORLDGUARD_ID);
        if (p == null || !(p instanceof WorldGuardPlugin)) {
            plugin.logger().warning("WorldGuard was not detected");
            return;
        }
        plugin.logger().info("using WorldGuard");
        worldGuard = (WorldGuardPlugin) p;
    }

    private void loadEconomyProvider() {
        plugin.logger().log("loading economy plugin");
        RegisteredServiceProvider<Economy> r = plugin.getServer().getServicesManager().getRegistration(Economy.class);
        EconomyProvider p = null;
        if (r == null) {
            p = new DefaultEconomyProvider(plugin);
        } else {
            p = new VaultEconomyProvider(r.getPlugin(), r.getProvider());
        }
        plugin.logger().info("using " + p.getProviderName() + "'s economy system");
        economyProvider = p;
    }

    private void loadPermissionProvider() {
        plugin.logger().log("loading permission provider");
        RegisteredServiceProvider<Permission> r = plugin.getServer().getServicesManager().getRegistration(Permission.class);
        PermissionProvider p = null;
        if (r == null) {
            p = new BukkitPermissionProvider();
        } else {
            p = new VaultPermissionProvider(r.getPlugin(), r.getProvider());
        }
        plugin.logger().info("using " + p.getProviderName() + "'s permission system");
        permissionProvider = p;
    }

    @Override
    public void load() {
        loadWorldGuard();
        loadEconomyProvider();
        loadPermissionProvider();
    }

    public boolean isWorldGuardInstalled() {
        return worldGuard != null;
    }

    public EconomyProvider economyProvider() {
        return economyProvider;
    }

    public PermissionProvider permissionProvider() {
        return permissionProvider;
    }

    @Override
    public String getID() {
        return "compatibility";
    }

    @Override
    public void save() throws IOException {}
}
