package mezzo.zz_bukkit.regionmanager.zz_old.economy;

import static mezzo.zz_bukkit.regionmanager.zz_old.configuration.ConfigManager.CONFIG_ECONOMY_CURRENCY_PLURAL;
import static mezzo.zz_bukkit.regionmanager.zz_old.configuration.ConfigManager.CONFIG_ECONOMY_CURRENCY_SINGULAR;
import static mezzo.zz_bukkit.regionmanager.zz_old.configuration.ConfigManager.CONFIG_ECONOMY_DEFAULTBALANCE;
import static mezzo.zz_bukkit.regionmanager.zz_old.configuration.ConfigManager.CONFIG_ECONOMY_ENABLED;
import static mezzo.zz_bukkit.regionmanager.zz_old.message.MessageManager.replacement;
import mezzo.zz_bukkit.regionmanager.zz_old.RegionManagerPlugin;
import mezzo.zz_bukkit.regionmanager.zz_old.economy.TransactionResult.Type;
import mezzo.zz_bukkit.regionmanager.zz_old.message.Messages;
import mezzo.zz_bukkit.regionmanager.zz_old.util.Player;

import com.avaje.ebean.EbeanServer;

/**
 * <code>InHouseEconomyProvider</code>
 * 
 * @author mezzodrinker
 */
public class DefaultEconomyProvider extends EconomyProvider {
    private final RegionManagerPlugin plugin;
    private final EbeanServer         database;

    public DefaultEconomyProvider(RegionManagerPlugin plugin) {
        if (plugin == null) throw new IllegalArgumentException("plugin can not be null");
        this.plugin = plugin;
        database = plugin.getDatabase();
    }

    private final Account getAccount(Player player) {
        Account account = database.find(Account.class).where().ieq("playerName", player.getName()).findUnique();
        if (account == null) {
            account = new Account();
            account.setPlayer(player.toBukkitPlayer());
            account.setMoney(plugin.configManager().economyConfig().getNumber(CONFIG_ECONOMY_DEFAULTBALANCE).doubleValue());
        }
        return account;
    }

    public boolean isEnabled() {
        return plugin.configManager().economyConfig().getBoolean(CONFIG_ECONOMY_ENABLED);
    }

    @Override
    public String currencyNameSingular() {
        return plugin.configManager().economyConfig().getString(CONFIG_ECONOMY_CURRENCY_SINGULAR);
    }

    @Override
    public String currencyNamePlural() {
        return plugin.configManager().economyConfig().getString(CONFIG_ECONOMY_CURRENCY_PLURAL);
    }

    @Override
    public boolean has(Player player, double amount) {
        return get(player) >= amount;
    }

    @Override
    public double get(Player player) {
        return getAccount(player).getMoney();
    }

    @Override
    public TransactionResult give(Player player, double amount) {
        Account account = getAccount(player);
        if (amount < 0) return new TransactionResult(0, account.getMoney(), Type.ERROR, plugin.messageManager().messageWrapped(Messages.Economy.giveNegative, replacement("<money>", amount)));
        account.setMoney(account.getMoney() + amount);
        database.save(account);
        return new TransactionResult(amount, account.getMoney());
    }

    @Override
    public TransactionResult take(Player player, double amount) {
        Account account = getAccount(player);
        if (amount < 0) return new TransactionResult(0, account.getMoney(), Type.ERROR, plugin.messageManager().messageWrapped(Messages.Economy.takeNegative, replacement("<money>", amount)));
        account.setMoney(account.getMoney() - amount);
        database.save(account);
        return new TransactionResult(-amount, account.getMoney());
    }

    @Override
    public String format(double amount) {
        int decimal = (int) amount;
        int floating = (int) ((amount - decimal) * Math.pow(10d, decimalPlaces()));
        String f = String.valueOf(floating);
        while (f.length() < decimalPlaces()) {
            f = "0" + f;
        }
        return String.valueOf(decimal) + "." + f + " " + (decimal == 1 && floating == 0 ? currencyNameSingular() : currencyNamePlural());
    }

    @Override
    public int decimalPlaces() {
        return 2;
    }

    @Override
    public String getProviderName() {
        return plugin.getName();
    }
}
