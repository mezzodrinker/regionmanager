package mezzo.zz_bukkit.regionmanager.zz_old.economy;

import mezzo.zz_bukkit.regionmanager.zz_old.util.Player;
import mezzo.zz_bukkit.regionmanager.zz_old.util.Provider;

/**
 * <code>EconomyProvider</code>
 * 
 * @author mezzodrinker
 */
public abstract class EconomyProvider extends Provider {
    public abstract String currencyNameSingular();

    public abstract String currencyNamePlural();

    public abstract boolean has(Player player, double amount);

    public abstract double get(Player player);

    public abstract TransactionResult give(Player player, double amount);

    public abstract TransactionResult take(Player player, double amount);

    public abstract String format(double amount);

    public abstract int decimalPlaces();
}
