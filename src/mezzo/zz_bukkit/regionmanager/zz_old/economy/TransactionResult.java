package mezzo.zz_bukkit.regionmanager.zz_old.economy;

import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.economy.EconomyResponse.ResponseType;

/**
 * <code>Foo</code>
 * 
 * @author mezzodrinker
 */
public final class TransactionResult {
    /**
     * amount modified by calling a method
     */
    public final double amount;
    /**
     * new account balance
     */
    public final double balance;
    /**
     * error message in case of type being Type.ERROR
     */
    public final String error;
    /**
     * success or failure of the transaction
     */
    public final Type   type;

    TransactionResult(double amount, double balance) {
        this.amount = amount;
        this.balance = balance;
        type = Type.SUCCESS;
        error = null;
    }

    TransactionResult(double amount, double balance, Type type, String error) {
        this.amount = amount;
        this.balance = balance;
        this.type = type;
        this.error = error;
    }

    public final boolean wasSuccessful() {
        return type == Type.SUCCESS;
    }

    public static final TransactionResult valueOf(EconomyResponse e) {
        return new TransactionResult(e.amount, e.balance, Type.valueOf(e.type), e.errorMessage);
    }

    public static enum Type {
        SUCCESS, ERROR, NOT_IMPLEMENTED;

        public static Type valueOf(ResponseType t) {
            switch (t) {
                case SUCCESS:
                    return Type.SUCCESS;
                case FAILURE:
                    return Type.ERROR;
                default:
                    return Type.NOT_IMPLEMENTED;
            }
        }
    }
}