package mezzo.zz_bukkit.regionmanager.zz_old.economy;

import mezzo.zz_bukkit.regionmanager.zz_old.util.Player;
import net.milkbowl.vault.economy.Economy;

import org.bukkit.plugin.Plugin;

/**
 * <code>VaultEconomyProvider</code>
 * 
 * @author mezzodrinker
 */
public class VaultEconomyProvider extends EconomyProvider {
    private Plugin  plugin  = null;
    private Economy economy = null;

    public VaultEconomyProvider(Plugin plugin, Economy economy) {
        if (plugin == null) throw new IllegalArgumentException("plugin can not be null");
        if (economy == null) throw new IllegalArgumentException("economy can not be null");
        this.plugin = plugin;
        this.economy = economy;
    }

    public Plugin getPlugin() {
        return plugin;
    }

    @Override
    public String format(double amount) {
        return economy.format(amount);
    }

    @Override
    public String getProviderName() {
        return plugin.getName();
    }

    @Override
    public String currencyNameSingular() {
        return economy.currencyNameSingular();
    }

    @Override
    public String currencyNamePlural() {
        return economy.currencyNamePlural();
    }

    @Override
    public boolean has(Player player, double amount) {
        return economy.has(player.getName(), amount);
    }

    @Override
    public double get(Player player) {
        return economy.getBalance(player.getName());
    }

    @Override
    public TransactionResult give(Player player, double amount) {
        if (amount < 0d) return take(player, -amount);
        return TransactionResult.valueOf(economy.depositPlayer(player.getName(), amount));
    }

    @Override
    public TransactionResult take(Player player, double amount) {
        if (amount < 0d) return give(player, -amount);
        return TransactionResult.valueOf(economy.withdrawPlayer(player.getName(), amount));
    }

    @Override
    public int decimalPlaces() {
        return economy.fractionalDigits();
    }
}
