package mezzo.zz_bukkit.regionmanager.zz_old.economy;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.avaje.ebean.validation.NotNull;

/**
 * <code>Account</code>
 * 
 * @author mezzodrinker
 */
@Entity
@Table(name = "rm_accounts")
public class Account {
    @Id
    private int    id;

    @NotNull
    private String playerName;

    @NotNull
    private double money;

    public int getId() {
        return id;
    }

    public Player getPlayer() {
        return Bukkit.getServer().getPlayer(playerName);
    }

    public double getMoney() {
        return money;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPlayer(Player player) {
        playerName = player.getName();
    }

    public void setMoney(double money) {
        this.money = money;
    }
}
