package mezzo.zz_bukkit.regionmanager.zz_old.region.flag;

import mezzo.util.configuration.serialization.ConfigurationSerializable;

/**
 * <code>Flag</code>
 * 
 * @param <T>
 *            the value type of this flag
 * @author mezzodrinker
 */
public abstract class Flag<T> implements ConfigurationSerializable {
    protected String name = null;

    protected Flag(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract T cast(Object o) throws ClassCastException;

    public abstract Object generalize(T o);
}
