package mezzo.zz_bukkit.regionmanager.zz_old.region;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import mezzo.bukkit.util.Vector3;
import mezzo.util.configuration.yaml.YamlConfiguration;
import mezzo.zz_bukkit.regionmanager.zz_old.RegionManagerPlugin;
import mezzo.zz_bukkit.regionmanager.zz_old.util.Player;

import org.bukkit.World;

/**
 * <code>RegionManager</code>
 * 
 * @author mezzodrinker
 */
public class RegionManager {
    private RegionManagerPlugin plugin  = null;
    private World               world   = null;
    private Region              global  = null;
    private Map<String, Region> regions = new TreeMap<String, Region>();

    /**
     * Global regions have no bounds - every block is part of them.
     */
    protected static final int  GLOBAL  = 0x00000001;
    /**
     * In public regions, everyone is a member.
     */
    protected static final int  PUBLIC  = 0x00000002;
    /**
     * Locked regions can't be entered.
     */
    protected static final int  LOCKED  = 0x00000004;

    public final File           fileRegions;
    public final File           fileGlobal;
    public final File           folderWorld;

    public RegionManager(RegionManagerPlugin plugin, World world) {
        this.plugin = plugin;
        this.world = world;
        folderWorld = plugin.getFile(new File(plugin.worldManager().folderWorlds, world.getName()));
        fileRegions = new File(folderWorld, "regions.yml");
        fileGlobal = new File(folderWorld, "global.yml");
    }

    public void load() {
        YamlConfiguration c = YamlConfiguration.loadConfiguration(fileRegions);
        for (String key : c.getKeys(false)) {
            add(c.getGeneric(key, Region.class));
        }

        c = YamlConfiguration.loadConfiguration(fileGlobal);
        for (String key : c.getKeys(false)) {
            global = c.getGeneric(key, Region.class);
        }

        if (global == null) {
            global = new Region("__global__", new Vector3(0, 0, 0), new Vector3(0, 0, 0), Integer.MIN_VALUE);
        }
        global.settings().setModifier(GLOBAL);
        global.settings().setModifier(PUBLIC);
    }

    public void save() throws IOException {
        YamlConfiguration c = new YamlConfiguration();
        for (Entry<String, Region> entry : regions.entrySet()) {
            c.set(entry.getKey(), entry.getValue());
        }
        c.save(fileRegions);

        c = new YamlConfiguration();
        c.set(global.getID(), global);
        c.save(fileGlobal);
    }

    public void add(Region region) {
        if (!regions.containsValue(region)) {
            regions.put(region.getID(), region);
        }
    }

    public File getFile() {
        return new File(folderWorld, "regions.yml");
    }

    public World getWorld() {
        return world;
    }

    public int size() {
        return regions.size();
    }

    public boolean exists(String region) {
        return regions.containsKey(region);
    }

    public Region getGlobalRegion() {
        return global;
    }

    public Region getRegion(String id) {
        return regions.get(id);
    }

    public Collection<Region> getRegions() {
        return regions.values();
    }

    public Collection<Region> getRegions(Player player) {
        Collection<Region> c = new HashSet<Region>();
        for (Region r : getRegions()) {
            // TODO add group support
            if (r.members().containsPlayer(player.getName()) || r.owners().containsPlayer(player.getName())) {
                c.add(r);
            }
        }
        return c;
    }

    public Collection<Region> getRegionsMember(Player player) {
        Collection<Region> c = new HashSet<Region>();
        for (Region r : getRegions()) {
            // TODO add group support
            if (r.members().containsPlayer(player.getName())) {
                c.add(r);
            }
        }
        return c;
    }

    public Collection<Region> getRegionsOwner(Player player) {
        Collection<Region> c = new HashSet<Region>();
        for (Region r : getRegions()) {
            // TODO add group support
            if (r.owners().containsPlayer(player.getName())) {
                c.add(r);
            }
        }
        return c;
    }

    public int getRegionsCount(Player player) {
        return getRegions(player).size();
    }

    public int getRegionsCountMember(Player player) {
        return getRegionsMember(player).size();
    }

    public int getRegionsCountOwner(Player player) {
        return getRegionsOwner(player).size();
    }

    public boolean remove(String region) {
        // TODO region inheritance support
        return regions.remove(region) != null;
    }

    public boolean remove(Region region) {
        return remove(region.getID());
    }

    public static boolean isGlobal(Region region) {
        return region.settings().hasModifier(GLOBAL);
    }

    public static boolean isPublic(Region region) {
        return region.settings().hasModifier(PUBLIC);
    }

    public static boolean isLocked(Region region) {
        return region.settings().hasModifier(LOCKED);
    }
}
