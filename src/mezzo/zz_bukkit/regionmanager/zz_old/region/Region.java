package mezzo.zz_bukkit.regionmanager.zz_old.region;

import static mezzo.zz_bukkit.regionmanager.zz_old.util.SerializationHelper.asAccessGroup;
import static mezzo.zz_bukkit.regionmanager.zz_old.util.SerializationHelper.asRegionSettings;
import static mezzo.zz_bukkit.regionmanager.zz_old.util.SerializationHelper.asString;
import static mezzo.zz_bukkit.regionmanager.zz_old.util.SerializationHelper.asVector3;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

import mezzo.util.configuration.serialization.ConfigurationSerializable;
import mezzo.util.configuration.serialization.ConfigurationSerializer;
import mezzo.util.configuration.serialization.SerializeAs;
import mezzo.util.math.Vector3;

import org.bukkit.entity.Player;

/**
 * <code>Region</code>
 * 
 * @author mezzodrinker
 */
@SerializeAs("Region")
public class Region implements ConfigurationSerializable, Comparable<Region> {
    private String                 id                     = null;
    private AccessGroup            members                = null;
    private AccessGroup            owners                 = null;
    private Vector3                locationMin            = null;
    private Vector3                locationMax            = null;
    private RegionSettings         settings               = null;

    protected static final String  SERIALIZE_ID           = "id";
    protected static final String  SERIALIZE_LOCATION_MIN = "location.min";
    protected static final String  SERIALIZE_LOCATION_MAX = "location.max";
    protected static final String  SERIALIZE_SETTINGS     = "settings";
    protected static final String  SERIALIZE_MEMBERS      = "members";
    protected static final String  SERIALIZE_OWNERS       = "owners";
    protected static final Pattern REGIONNAME             = Pattern.compile("^(\\w|[\\-\\+/])++$");

    static {
        ConfigurationSerializer.registerClass(Region.class);
    }

    public Region(String id, Vector3 vectorA, Vector3 vectorB) {
        this(id, vectorA, vectorB, 0);
    }

    public Region(String id, Vector3 vectorA, Vector3 vectorB, int priority) {
        if (id == null) throw new IllegalArgumentException("ID can not be null");
        if (vectorA == null) throw new IllegalArgumentException("vector a can not be null");
        if (vectorB == null) throw new IllegalArgumentException("vector b can not be null");
        if (!isValid(id)) throw new IllegalArgumentException("invalid ID: region IDs can only contain a-z, A-Z, 0-9, underscores, - and +");

        this.id = id;

        double x = Math.min(vectorA.x, vectorB.x);
        double y = Math.min(vectorA.y, vectorB.y);
        double z = Math.min(vectorA.z, vectorB.z);
        locationMin = new Vector3(x, y, z);

        x = Math.max(vectorA.x, vectorB.x);
        y = Math.max(vectorA.y, vectorB.y);
        z = Math.max(vectorA.z, vectorB.z);
        locationMax = new Vector3(x, y, z);

        members = new AccessGroup();
        owners = new AccessGroup();
        settings = new RegionSettings();

        settings.setPriority(priority);
    }

    protected void settings(RegionSettings settings) {
        this.settings = settings;
    }

    public AccessGroup members() {
        return members;
    }

    public void members(AccessGroup members) {
        this.members = members;
    }

    public AccessGroup owners() {
        return owners;
    }

    public void owners(AccessGroup owners) {
        this.owners = owners;
    }

    public boolean isMember(Player player) {
        return members.containsPlayer(player.getName()) || RegionManager.isGlobal(this) || RegionManager.isPublic(this);
    }

    public boolean isOwner(Player player) {
        return owners.containsPlayer(player.getName());
    }

    public String getID() {
        return id;
    }

    public Vector3 getMinLocation() {
        return locationMin;
    }

    public Vector3 getMaxLocation() {
        return locationMax;
    }

    public boolean contains(Vector3 v) {
        return v.x >= locationMin.getBlockX() && v.x < locationMax.getBlockX() + 1 && v.y >= locationMin.getBlockY() && v.y < locationMax.getBlockY() + 1 && v.y >= locationMin.getBlockZ()
                && v.z < locationMax.getBlockZ() + 1;
    }

    public RegionSettings settings() {
        return settings;
    }

    public void copySettings(Region region) {
        settings(region.settings().clone());
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Region)) return false;
        return ((Region) o).getID().equals(getID());
    }

    @Override
    public int compareTo(Region r) {
        if (r == null) return 1;
        return settings().getPriority() - r.settings().getPriority();
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        map.put(SERIALIZE_ID, id);
        map.put(SERIALIZE_LOCATION_MIN, locationMin);
        map.put(SERIALIZE_LOCATION_MAX, locationMax);
        map.put(SERIALIZE_MEMBERS, members);
        map.put(SERIALIZE_OWNERS, owners);
        map.put(SERIALIZE_SETTINGS, settings);
        return map;
    }

    public static boolean isValid(String regionID) {
        return REGIONNAME.matcher(regionID).matches();
    }

    public static Region deserialize(Map<String, Object> map) {
        String id = asString(map, SERIALIZE_ID);
        Vector3 locationMin = asVector3(map, SERIALIZE_LOCATION_MIN);
        Vector3 locationMax = asVector3(map, SERIALIZE_LOCATION_MAX);
        Region r = new Region(id, locationMin, locationMax);
        r.settings(asRegionSettings(map, SERIALIZE_SETTINGS));
        r.members(asAccessGroup(map, SERIALIZE_MEMBERS));
        r.owners(asAccessGroup(map, SERIALIZE_OWNERS));
        return r;
    }
}
