package mezzo.zz_bukkit.regionmanager.zz_old.region;

import static mezzo.zz_bukkit.regionmanager.zz_old.util.SerializationHelper.asFlagList;
import static mezzo.zz_bukkit.regionmanager.zz_old.util.SerializationHelper.asInteger;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import mezzo.util.configuration.serialization.ConfigurationSerializable;
import mezzo.util.configuration.serialization.ConfigurationSerializer;
import mezzo.util.configuration.serialization.SerializeAs;
import mezzo.zz_bukkit.regionmanager.zz_old.region.flag.Flag;

/**
 * <code>RegionSettings</code>
 * 
 * @author mezzodrinker
 */
@SerializeAs("RegionSettings")
public class RegionSettings implements ConfigurationSerializable, Cloneable {
    private int                   priority           = 0;
    private int                   modifiers          = 0;
    private Map<Flag<?>, Object>  flags              = new HashMap<Flag<?>, Object>();

    protected static final String SERIALIZE_PRIORITY = "priority";
    protected static final String SERIALIZE_FLAGS    = "flags";

    static {
        ConfigurationSerializer.registerClass(RegionSettings.class);
    }

    public RegionSettings() {}

    public <T> T getFlag(Flag<T> flag) {
        if (flag == null) return null;
        Object o = flags.get(flag);
        if (o == null) return null;
        return flag.cast(o);
    }

    public <T> T setFlag(Flag<T> flag, T value) {
        if (flag == null) return null;
        Object o = flags.get(flag);
        flags.put(flag, value);
        if (o == null) return null;
        return flag.cast(o);
    }

    public void setFlags(Map<? extends Flag<?>, Object> map) {
        flags.putAll(map);
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }

    public void setModifier(int modifier) {
        if (!hasModifier(modifier)) {
            modifiers += modifier;
        }
    }

    public void removeModifier(int modifier) {
        if (hasModifier(modifier)) {
            modifiers -= modifier;
        }
    }

    public boolean hasModifier(int modifier) {
        return (modifiers & modifier) != 0;
    }

    @Override
    public RegionSettings clone() {
        RegionSettings r = new RegionSettings();
        r.setFlags(flags);
        r.setPriority(priority);
        return r;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        map.put(SERIALIZE_PRIORITY, priority);
        for (Entry<Flag<?>, Object> entry : flags.entrySet()) {
            map.put(SERIALIZE_FLAGS + "." + entry.getKey().getName(), entry.getValue());
        }
        return Collections.unmodifiableMap(map);
    }

    public static RegionSettings deserialize(Map<String, Object> map) {
        RegionSettings r = new RegionSettings();
        r.setPriority(asInteger(map, SERIALIZE_PRIORITY));
        r.setFlags(asFlagList(map, SERIALIZE_FLAGS));
        return new RegionSettings();
    }
}
