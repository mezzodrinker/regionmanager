package mezzo.zz_bukkit.regionmanager.zz_old.region;

import static mezzo.zz_bukkit.regionmanager.zz_old.util.SerializationHelper.asStringList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mezzo.util.configuration.serialization.ConfigurationSerializable;
import mezzo.util.configuration.serialization.ConfigurationSerializer;
import mezzo.util.configuration.serialization.SerializeAs;

/**
 * <code>AccessGroup</code>
 * 
 * @author mezzodrinker
 */
@SerializeAs("AccessGroup")
public class AccessGroup implements ConfigurationSerializable {
    private List<String>          players           = new ArrayList<String>();
    private List<String>          groups            = new ArrayList<String>();

    protected static final String SERIALIZE_PLAYERS = "players";
    protected static final String SERIALIZE_GROUPS  = "groups";

    static {
        ConfigurationSerializer.registerClass(AccessGroup.class);
    }

    public void addPlayer(String player) {
        if (!players.contains(player)) {
            players.add(player);
        }
    }

    public void addGroup(String group) {
        if (!groups.contains(group)) {
            groups.add(group);
        }
    }

    public boolean removePlayer(String player) {
        return players.remove(player);
    }

    public boolean removeGroup(String group) {
        return groups.remove(group);
    }

    public boolean containsPlayer(String player) {
        return players.contains(player);
    }

    public boolean containsGroup(String group) {
        return groups.contains(group);
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        map.put(SERIALIZE_PLAYERS, players);
        map.put(SERIALIZE_GROUPS, groups);
        return Collections.unmodifiableMap(map);
    }

    public static AccessGroup deserialize(Map<String, Object> map) {
        AccessGroup a = new AccessGroup();
        for (String s : asStringList(map, SERIALIZE_PLAYERS)) {
            a.addPlayer(s);
        }
        for (String s : asStringList(map, SERIALIZE_GROUPS)) {
            a.addGroup(s);
        }
        return a;
    }
}