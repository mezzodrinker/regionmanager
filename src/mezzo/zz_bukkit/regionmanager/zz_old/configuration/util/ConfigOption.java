package mezzo.zz_bukkit.regionmanager.zz_old.configuration.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <code>ConfigOption</code>
 * 
 * @author mezzodrinker
 */
public class ConfigOption {
    private String       key          = null;
    private Object       defaultValue = null;
    private List<String> oldKeys      = null;
    private DataType     dataType     = null;
    private String       inGame       = null;

    public ConfigOption(String key, Object defaultValue) {
        this(key, defaultValue, null);
    }

    public ConfigOption(String key, Object defaultValue, List<String> oldKeys) {
        this(key, defaultValue, oldKeys, null);
    }

    public ConfigOption(String key, Object defaultValue, List<String> oldKeys, DataType dataType) {
        this(key, defaultValue, oldKeys, dataType, null);
    }

    public ConfigOption(String key, Object defaultValue, List<String> oldKeys, DataType dataType, String inGame) {
        if (key == null) throw new IllegalArgumentException("key can not be null");

        this.key = key;
        this.defaultValue = defaultValue;
        this.oldKeys = oldKeys;
        this.dataType = dataType;
        this.inGame = inGame;
    }

    public String getKey() {
        return key;
    }

    public List<String> getOldKeys() {
        return oldKeys;
    }

    public List<String> getKeys() {
        if (oldKeys == null) {
            oldKeys = new ArrayList<String>();
        }
        if (!oldKeys.contains(key)) {
            oldKeys.add(key);
        }
        return oldKeys;
    }

    public String getInGameKey() {
        return inGame;
    }

    public Object getDefaultValue() {
        return defaultValue;
    }

    public DataType getDataType() {
        return dataType;
    }

    @Override
    public String toString() {
        return "ConfigOption:{key:\'" + key + "\';defaultValue:\'" + defaultValue + "\';dataType:\'" + dataType + "\';oldKeys:\'" + oldKeys == null ? "null" : Arrays.toString(oldKeys.toArray())
                + "\'}";
    }

    public static enum DataType {
        OBJECT, BOOLEAN, BOOLEAN_LIST, BYTE_LIST, CONFIGURATIONSECTION, CHARACTER_LIST, COLOR, DOUBLE, DOUBLE_LIST, FLOAT_LIST, INT, INTEGER_LIST, ITEMSTACK, LIST, LONG, LONG_LIST, MAP_LIST, OFFLINEPLAYER, SHORT_LIST, STRING, STRING_LIST, VECTOR;
    }
}
