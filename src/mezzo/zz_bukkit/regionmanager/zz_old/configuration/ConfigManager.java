package mezzo.zz_bukkit.regionmanager.zz_old.configuration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import mezzo.bukkit.configuration.IConfigManager;
import mezzo.util.configuration.yaml.YamlConfiguration;
import mezzo.zz_bukkit.regionmanager.zz_old.Component;
import mezzo.zz_bukkit.regionmanager.zz_old.RegionManagerPlugin;
import mezzo.zz_bukkit.regionmanager.zz_old.configuration.util.ConfigOption;
import mezzo.zz_bukkit.regionmanager.zz_old.configuration.util.ConfigOption.DataType;

/**
 * <code>ConfigManager</code>
 * 
 * @author mezzodrinker
 */
public class ConfigManager implements IConfigManager, Component {
    private YamlConfiguration                     config                            = null;
    private YamlConfiguration                     configRegions                     = null;
    private YamlConfiguration                     configEconomy                     = null;
    private HashMap<String, YamlConfiguration>    configsPlayer                     = new HashMap<String, YamlConfiguration>();

    public final File                             folderPlayers;
    public final File                             fileMain;
    public final File                             fileRegions;
    public final File                             fileEconomy;

    public static final int                       MAIN_CONFIG                       = 0x1;
    public static final int                       REGIONS_CONFIG                    = 0x2;
    public static final int                       PLAYER_CONFIG                     = 0x4;

    // Configuration options of the main configuration (config.yml)
    public static final String                    CONFIG_MAIN_VERSION               = "version";
    public static final String                    CONFIG_MAIN_LANG                  = "language";
    public static final String                    CONFIG_MAIN_UPDATER_PERFORMCHECKS = "updater.perform update checks";
    public static final String                    CONFIG_MAIN_UPDATER_DOWNLOAD      = "updater.download updates";
    public static final String                    CONFIG_MAIN_WORLDS_DISABLED       = "worlds.disable RegionManager on";

    // Configuration options of the regions configuration (regions.yml)

    // Configuration options of the economy configuration (economy.yml)
    public static final String                    CONFIG_ECONOMY_ENABLED            = "enabled";
    public static final String                    CONFIG_ECONOMY_CURRENCY_SINGULAR  = "currency name.singular";
    public static final String                    CONFIG_ECONOMY_CURRENCY_PLURAL    = "currency name.plural";
    public static final String                    CONFIG_ECONOMY_DEFAULTBALANCE     = "default balance";

    // public static final String CONFIG_MAIN_DEBUG = "debug";
    // public static final String CONFIG_MAIN_SEIZING_ENABLED = "auto-seizing.enabled";
    // public static final String CONFIG_MAIN_SEIZING_TIME = "auto-seizing.offline time";
    // public static final String CONFIG_MAIN_SEIZING_PERIOD = "auto-seizing.period";
    // public static final String CONFIG_MAIN_SEIZING_ASK = "auto-seizing.permission to save data needed";
    // public static final String CONFIG_MAIN_SEIZING_SUBSTITUTE = "auto-seizing.substitute owner";
    // public static final String CONFIG_MAIN_SEIZING_BLACKLIST_PLAYER_ENABLED = "auto-seizing.player blacklist.enabled";
    // public static final String CONFIG_MAIN_SEIZING_BLACKLIST_PLAYER_LIST = "auto-seizing.player blacklist.blacklist";
    // public static final String CONFIG_MAIN_SEIZING_BLACKLIST_WORLD_ENABLED = "auto-seizing.world blacklist.enabled";
    // public static final String CONFIG_MAIN_SEIZING_BLACKLIST_WORLD_LIST = "auto-seizing.world blacklist.blacklist";

    // Configuration options of the regions configuration (regions.yml)
    // public static final String CONFIG_REGIONS_BUY_MARK_ENABLED = "buying.marking.enabled";
    // public static final String CONFIG_REGIONS_BUY_MARK_ID = "buying.marking.block ID";
    // public static final String CONFIG_REGIONS_BUY_SERVER_PRICE = "buying.from server.price per region";
    // public static final String CONFIG_REGIONS_BUY_SERVER_INCREASE_ENABLED = "buying.from server.increasing price.enabled";
    // public static final String CONFIG_REGIONS_BUY_SERVER_INCREASE_PERCENTAGE = "buying.from server.increasing price.by percentage";
    // public static final String CONFIG_REGIONS_BUY_SERVER_INCREASE_AMOUNT = "buying.from server.increasing price.by amount";
    // public static final String CONFIG_REGIONS_BUY_SERVER_INCREASE_CAP = "buying.from server.increasing price.cap at";
    // public static final String CONFIG_REGIONS_BUY_SERVER_ACCOUNT = "buying.from server.account";
    // public static final String CONFIG_REGIONS_BUY_PLAYER_PRICE = "buying.from player.default price";
    // public static final String CONFIG_REGIONS_BUY_PLAYER_INCREASE_ENABLED = "buying.from player.increasing price.enabled";
    // public static final String CONFIG_REGIONS_BUY_PLAYER_INCREASE_PERCENTAGE = "buying.from player.increasing price.by percentage";
    // public static final String CONFIG_REGIONS_BUY_PLAYER_INCREASE_AMOUNT = "buying.from player.increasing price.by amount";
    // public static final String CONFIG_REGIONS_BUY_PLAYER_INCREASE_CAP = "buying.from player.increasing price.cap";
    // public static final String CONFIG_REGIONS_BUY_PLAYER_TAXES_ENABLED = "buying.from player.taxes.enabled";
    // public static final String CONFIG_REGIONS_BUY_PLAYER_TAXES_PERCENTAGE = "buying.from player.taxes.percentage";
    // public static final String CONFIG_REGIONS_BUY_PLAYER_TAXES_ACCOUNT = "buying.from player.taxes.account";
    // public static final String CONFIG_REGIONS_SELL_MARK_ENABLED = "selling.marking.enabled";
    // public static final String CONFIG_REGIONS_SELL_MARK_ID = "selling.marking.block ID";
    // public static final String CONFIG_REGIONS_SELL_SERVER_AMOUNT = "selling.to server.amount per region";
    // public static final String CONFIG_REGIONS_SELL_PLAYER_PRICE = "selling.to player.default price";
    // public static final String CONFIG_REGIONS_SELL_PLAYER_TAXES_ENABLED = "selling.to player.taxes.enabled";
    // public static final String CONFIG_REGIONS_SELL_PLAYER_TAXES_PERCENTAGE = "selling.to player.taxes.percentage";
    // public static final String CONFIG_REGIONS_SELL_PLAYER_TAXES_ACCOUNT = "selling.to player.taxes.account";
    // public static final String CONFIG_REGIONS_CREATE_ENABLED = "auto-creation.enabled";
    // public static final String CONFIG_REGIONS_CREATE_OVERLAY = "auto-creation.overlay existing regions";
    // public static final String CONFIG_REGIONS_CREATE_FLAGS = "auto-creation.default flags";
    // public static final String CONFIG_REGIONS_CREATE_SIZE_X = "auto-creation.region size.x";
    // public static final String CONFIG_REGIONS_CREATE_SIZE_Y = "auto-creation.region size.y";
    // public static final String CONFIG_REGIONS_CREATE_SIZE_Z = "auto-creation.region size.z";
    // public static final String CONFIG_REGIONS_CREATE_BASE_X = "auto-creation.base point.x";
    // public static final String CONFIG_REGIONS_CREATE_BASE_Y = "auto-creation.base point.y";
    // public static final String CONFIG_REGIONS_CREATE_BASE_Z = "auto-creation.base point.z";
    // public static final String CONFIG_REGIONS_CREATE_BLACKLIST_WORLD_ENABLED = "auto-creation.world blacklist.enabled";
    // public static final String CONFIG_REGIONS_CREATE_BLACKLIST_WORLD_WHITE = "auto-creation.world blacklist.use as whitelist";
    // public static final String CONFIG_REGIONS_CREATE_BLACKLIST_WORLD_LIST = "auto-creation.world blacklist.blacklist";
    // public static final String CONFIG_REGIONS_LOCK_ENABLED = "locking.enabled";
    // public static final String CONFIG_REGIONS_LOCK_FLAGS = "locking.flags";
    // public static final String CONFIG_REGIONS_LOCK_BLACKLIST_WORLD_ENABLED = "locking.world blacklist.enabled";
    // public static final String CONFIG_REGIONS_LOCK_BLACKLIST_WORLD_WHITE = "locking.world blacklist.use as whitelist";
    // public static final String CONFIG_REGIONS_LOCK_BLACKLIST_LIST = "locking.world blacklist.blacklist";
    // public static final String CONFIG_REGIONS_SIGN_LINES_LINE0 = "signs.lines.line 1";
    // public static final String CONFIG_REGIONS_SIGN_LINES_LINE1 = "signs.lines.line 2";
    // public static final String CONFIG_REGIONS_SIGN_LINES_LINE2 = "signs.lines.line 3";
    // public static final String CONFIG_REGIONS_SIGN_LINES_LINE3 = "signs.lines.line 4";

    // Configuration options of each player's configuration
    // public static final String CONFIG_PLAYER_NAME = "player name";
    // public static final String CONFIG_PLAYER_LASTLOGOUT = "last logout";

    // Configuration options map; mapped by current configuration option
    public static final Map<String, ConfigOption> CONFIG_MAIN_OPTIONS               = new LinkedHashMap<String, ConfigOption>();
    public static final Map<String, ConfigOption> CONFIG_REGIONS_OPTIONS            = new LinkedHashMap<String, ConfigOption>();
    public static final Map<String, ConfigOption> CONFIG_ECONOMY_OPTIONS            = new LinkedHashMap<String, ConfigOption>();
    public static final Map<String, ConfigOption> CONFIG_PLAYER_OPTIONS             = new LinkedHashMap<String, ConfigOption>();

    // Configuration options mapped by data type
    private static final HashSet<String>          DATA_TYPES_BOOLEAN                = new HashSet<String>();
    private static final HashSet<String>          DATA_TYPES_COLOR                  = new HashSet<String>();
    private static final HashSet<String>          DATA_TYPES_DOUBLE                 = new HashSet<String>();
    private static final HashSet<String>          DATA_TYPES_INT                    = new HashSet<String>();
    private static final HashSet<String>          DATA_TYPES_ITEMSTACK              = new HashSet<String>();
    private static final HashSet<String>          DATA_TYPES_LIST                   = new HashSet<String>();
    private static final HashSet<String>          DATA_TYPES_LONG                   = new HashSet<String>();
    private static final HashSet<String>          DATA_TYPES_OFFLINEPLAYER          = new HashSet<String>();
    private static final HashSet<String>          DATA_TYPES_STRING                 = new HashSet<String>();
    private static final HashSet<String>          DATA_TYPES_VECTOR                 = new HashSet<String>();

    static {
        final String DEFAULT_VALUE = ".default";
        final String DATA_TYPE = ".data type";
        final String OLD_KEYS = ".old keys";
        final String INGAME = ".in-game";

        InputStream configOptionsStream = ConfigManager.class.getClassLoader().getResourceAsStream("config.yml");
        YamlConfiguration configOptions = YamlConfiguration.loadConfiguration(configOptionsStream);
        Set<String> keys = configOptions.getKeys(true);

        for (String key : keys) {
            if (keys.contains(key + DEFAULT_VALUE) && keys.contains(key + DATA_TYPE)) {
                Object defaultValue = configOptions.get(key + DEFAULT_VALUE);
                DataType dataType = DataType.valueOf(configOptions.getString(key + DATA_TYPE));
                String inGame = configOptions.getString(key + INGAME);
                List<String> oldKeys = configOptions.getStringList(key + OLD_KEYS);

                CONFIG_MAIN_OPTIONS.put(key, new ConfigOption(key, defaultValue, oldKeys, dataType, inGame));
                register(key, dataType);
            }
        }

        configOptionsStream = ConfigManager.class.getClassLoader().getResourceAsStream("regions.yml");
        configOptions = YamlConfiguration.loadConfiguration(configOptionsStream);
        keys = configOptions.getKeys(true);

        for (String key : keys) {
            if (keys.contains(key + DEFAULT_VALUE) && keys.contains(key + DATA_TYPE)) {
                Object defaultValue = configOptions.get(key + DEFAULT_VALUE);
                DataType dataType = DataType.valueOf(configOptions.getString(key + DATA_TYPE));
                String inGame = configOptions.getString(key + INGAME);
                List<String> oldKeys = configOptions.getStringList(key + OLD_KEYS);

                CONFIG_REGIONS_OPTIONS.put(key, new ConfigOption(key, defaultValue, oldKeys, dataType, inGame));
                register(key, dataType);
            }
        }

        configOptionsStream = ConfigManager.class.getClassLoader().getResourceAsStream("economy.yml");
        configOptions = YamlConfiguration.loadConfiguration(configOptionsStream);
        keys = configOptions.getKeys(true);

        for (String key : keys) {
            if (keys.contains(key + DEFAULT_VALUE) && keys.contains(key + DATA_TYPE)) {
                Object defaultValue = configOptions.get(key + DEFAULT_VALUE);
                DataType dataType = DataType.valueOf(configOptions.getString(key + DATA_TYPE));
                String inGame = configOptions.getString(key + INGAME);
                List<String> oldKeys = configOptions.getStringList(key + OLD_KEYS);

                CONFIG_ECONOMY_OPTIONS.put(key, new ConfigOption(key, defaultValue, oldKeys, dataType, inGame));
                register(key, dataType);
            }
        }

        configOptionsStream = ConfigManager.class.getClassLoader().getResourceAsStream("player.yml");
        configOptions = YamlConfiguration.loadConfiguration(configOptionsStream);
        keys = configOptions.getKeys(true);

        for (String key : keys) {
            if (keys.contains(key + DATA_TYPE)) {
                Object defaultValue = configOptions.get(key + DEFAULT_VALUE);
                DataType dataType = DataType.valueOf(configOptions.getString(key + DATA_TYPE));
                List<String> oldKeys = configOptions.getStringList(key + OLD_KEYS);
                CONFIG_PLAYER_OPTIONS.put(key, new ConfigOption(key, defaultValue, oldKeys, dataType));
            }
        }
    }

    public ConfigManager(RegionManagerPlugin plugin) {
        if (plugin == null) throw new IllegalArgumentException("plugin can not be null");

        folderPlayers = plugin.getFile("players");
        fileMain = plugin.getFile("config.yml");
        fileRegions = plugin.getFile("regions.yml");
        fileEconomy = plugin.getFile("economy.yml");
    }

    void setConfig(YamlConfiguration config) {
        this.config = config;
    }

    void setRegionsConfig(YamlConfiguration config) {
        configRegions = config;
    }

    void setEconomyConfig(YamlConfiguration config) {
        configEconomy = config;
    }

    void setPlayersConfig(String name, YamlConfiguration config) {
        configsPlayer.put(name, config);
    }

    @Override
    public void loadConfig() throws IOException {
        config = YamlConfiguration.loadConfiguration(fileMain);
        configRegions = YamlConfiguration.loadConfiguration(fileRegions);
        configEconomy = YamlConfiguration.loadConfiguration(fileEconomy);
    }

    @Override
    public void saveConfig() throws IOException {
        config.save(fileMain);
        configRegions.save(fileRegions);
        configEconomy.save(fileEconomy);

        for (Entry<String, YamlConfiguration> entry : configsPlayer.entrySet()) {
            entry.getValue().save(new File(folderPlayers.getPath(), entry.getKey() + ".yml"));
        }
    }

    @Override
    public YamlConfiguration getConfig() {
        return config();
    }

    public YamlConfiguration config() {
        return config;
    }

    public YamlConfiguration regionsConfig() {
        return configRegions;
    }

    public YamlConfiguration economyConfig() {
        return configEconomy;
    }

    public YamlConfiguration playerConfig(String name) {
        if (!configsPlayer.containsKey(name)) {
            configsPlayer.put(name, YamlConfiguration.loadConfiguration(new File(folderPlayers.getPath(), name + ".yml")));
        }
        return configsPlayer.get(name);
    }

    public String getType(String key) {
        if (DATA_TYPES_BOOLEAN.contains(key)) return "boolean";
        if (DATA_TYPES_COLOR.contains(key)) return "color";
        if (DATA_TYPES_DOUBLE.contains(key)) return "floating point";
        if (DATA_TYPES_INT.contains(key)) return "integer";
        if (DATA_TYPES_ITEMSTACK.contains(key)) return "item";
        if (DATA_TYPES_LIST.contains(key)) return "list";
        if (DATA_TYPES_LONG.contains(key)) return "number";
        if (DATA_TYPES_OFFLINEPLAYER.contains(key)) return "player";
        if (DATA_TYPES_STRING.contains(key)) return "string";
        if (DATA_TYPES_VECTOR.contains(key)) return "vector";
        return "unknown type";
    }

    /**
     * Returns the in-game key of a configuration option based on the key of that option.
     * 
     * @param key
     *            the key of the configuration option
     * @return the in-game key. <code>null</code> will be returned if the specified key is not supported.
     */
    public String getInGameKey(String key) {
        return getConfigOption(key).getInGameKey();
    }

    /**
     * Returns a <code>ConfigOption</code> instance for the given in-game key.
     * 
     * @param ingameKey
     *            the <code>ConfigOpion</code>'s in-game key
     * @return <code>null</code> if the key was not found in an configuration, a <code>ConfigOption</code> if the key was found.
     */
    public ConfigOption getInGameConfigOption(String ingameKey) {
        for (ConfigOption c : CONFIG_MAIN_OPTIONS.values()) {
            if (c.getInGameKey().equals((ingameKey = ingameKey.toLowerCase()))) return c;
        }
        for (ConfigOption c : CONFIG_REGIONS_OPTIONS.values()) {
            if (c.getInGameKey().equals((ingameKey = ingameKey.toLowerCase()))) return c;
        }
        return null;
    }

    /**
     * Returns a <code>ConfigOption</code> instance for the given key.
     * 
     * @param key
     *            the <code>ConfigOption</code>'s configuration key
     * @return <code>null</code> if the key was not found in any configuration, a <code>ConfigOption</code> if the key was found.
     */
    public ConfigOption getConfigOption(String key) {
        ConfigOption resultMain = getMainConfigOption(key);
        ConfigOption resultRegions = getRegionsConfigOption(key);
        ConfigOption resultPlayers = getPlayerConfigOption(key);

        return resultMain != null ? resultMain : (resultRegions != null ? resultRegions : resultPlayers);
    }

    /**
     * Returns a <code>ConfigOption</code> instance for the given key.
     * 
     * @param key
     *            the <code>ConfigOption</code>'s configuration key
     * @return <code>null</code> if the key was not found in the main configuration, a <code>ConfigOption</code> if the key was found.
     */
    public ConfigOption getMainConfigOption(String key) {
        return CONFIG_MAIN_OPTIONS.get(key);
    }

    /**
     * Returns a <code>ConfigOption</code> instance for the given key.
     * 
     * @param key
     *            the <code>ConfigOption</code>'s configuration key
     * @return <code>null</code> if the key was not found in the regions configuration, a <code>ConfigOption</code> if the key was found.
     */
    public ConfigOption getRegionsConfigOption(String key) {
        return CONFIG_REGIONS_OPTIONS.get(key);
    }

    /**
     * Returns a <code>ConfigOption</code> instance for the given key.
     * 
     * @param key
     *            the <code>ConfigOption</code>'s configuration key
     * @return <code>null</code> if the key was not found in the player configuration, a <code>ConfigOption</code> if the key was found.
     */
    public ConfigOption getPlayerConfigOption(String key) {
        return CONFIG_PLAYER_OPTIONS.get(key);
    }

    public int getConfigIDFor(String key) {
        if (getConfigOption(key) != null) return MAIN_CONFIG;
        if (getRegionsConfigOption(key) != null) return REGIONS_CONFIG;
        if (getPlayerConfigOption(key) != null) return PLAYER_CONFIG;
        return -1;
    }

    private static void register(String key, DataType dataType) {
        switch (dataType) {
            case BOOLEAN:
                DATA_TYPES_BOOLEAN.add(key);
                break;
            case COLOR:
                DATA_TYPES_COLOR.add(key);
                break;
            case DOUBLE:
                DATA_TYPES_DOUBLE.add(key);
                break;
            case INT:
                DATA_TYPES_INT.add(key);
                break;
            case ITEMSTACK:
                DATA_TYPES_ITEMSTACK.add(key);
                break;
            case LIST:
            case BOOLEAN_LIST:
            case BYTE_LIST:
            case CHARACTER_LIST:
            case DOUBLE_LIST:
            case FLOAT_LIST:
            case INTEGER_LIST:
            case LONG_LIST:
            case MAP_LIST:
            case SHORT_LIST:
            case STRING_LIST:
                DATA_TYPES_LIST.add(key);
                break;
            case LONG:
                DATA_TYPES_LONG.add(key);
                break;
            case OFFLINEPLAYER:
                DATA_TYPES_OFFLINEPLAYER.add(key);
                break;
            case STRING:
                DATA_TYPES_STRING.add(key);
                break;
            case VECTOR:
                DATA_TYPES_VECTOR.add(key);
                break;
            default:
                break;
        }
    }

    public static enum ConfigurationType {
        MAIN, REGIONS, PLAYER;
    }

    @Override
    public String getID() {
        return "configuration";
    }

    @Override
    public void load() throws IOException {
        loadConfig();
    }

    @Override
    public void save() throws IOException {
        saveConfig();
    }
}