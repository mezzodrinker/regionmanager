package mezzo.zz_bukkit.regionmanager.zz_old.message;

/**
 * <code>Messages</code>
 * 
 * @author mezzodrinker
 */
public final class Messages {
    public static final class AdditionalFiles {
        static final String        base         = "additional files";

        public static final String initializing = base + ".initializing";
    }

    public static final class CommandManager {
        static final String        base         = "command manager";

        public static final String initializing = base + ".initializing";
    }

    public static final class Command {
        static final String base = "command";

        public static final class Error {
            static final String        base                           = Command.base + ".error";

            public static final String notRecognized                  = base + ".command not recognized";
            public static final String insufficientPermissions        = base + ".insufficient permissions";
            public static final String insufficientPermissions_hidden = base + ".insufficient permissions (permission hidden)";
            public static final String tooFewArguments                = base + ".too few arguments";
            public static final String tooManyArguments               = base + ".too many arguments";
            public static final String exception                      = base + ".exception";

            public static final class Region {
                static final String        base    = Error.base + ".region";

                public static final String invalid = base + ".invalid";
            }

            public static final class World {
                static final String        base     = Error.base + ".world";

                public static final String disabled = base + ".disabled";
                public static final String invalid  = base + ".invalid";
            }
        }

        public static final class Help {
            static final String        base     = Command.base + ".help";

            public static final String note     = base + ".note";
            public static final String heading  = base + ".heading";
            public static final String nextPage = base + ".next page";

            public static final class RegionManager {
                static final String        base                  = Help.base + "./regionmanager";

                public static final String plugin                = base + "./regionmanager";
                public static final String help                  = base + "./regionmanager ?";
                public static final String config                = base + "./regionmanager config";
                public static final String config_help           = base + "./regionmanager config help";
                public static final String save                  = base + "./regionmanager save";
                public static final String reload                = base + "./regionmanager reload";
                public static final String region                = base + "./regionmanager region";
                public static final String region_add            = base + "./regionmanager region add";
                public static final String region_delete         = base + "./regionmanager region delete";
                public static final String region_members        = base + "./regionmanager region members";
                public static final String region_members_add    = base + "./regionmanager region members add";
                public static final String region_members_remove = base + "./regionmanager region members remove";
                public static final String region_owners         = base + "./regionmanager region owners";
                public static final String region_owners_add     = base + "./regionmanager region owners add";
                public static final String region_owners_remove  = base + "./regionmanager region owners remove";
            }
        }
    }

    public static final class CompatManager {
        static final String        base         = "compatibility manager";

        public static final String initializing = base + ".initializing";

        public static final class Missing_plugin {
            static final String        base        = CompatManager.base + "missing plugin";

            public static final String permissions = base + ".permissions";
            public static final String economy     = base + ".economy";
            public static final String worldguard  = base + ".worldguard";
        }
    }

    public static final class Component {
        static final String        base           = "component";

        public static final String notFound       = base + ".not found";
        public static final String notFound_regex = base + ".not found (regex)";

        public static final class Reload {
            static final String        base      = Component.base + ".reload";

            public static final String started   = base + ".started";
            public static final String succeeded = base + ".succeeded";
            public static final String failed    = base + ".failed";
        }

        public static final class Save {
            static final String        base      = Component.base + ".save";

            public static final String started   = base + ".started";
            public static final String succeeded = base + ".succeeded";
            public static final String failed    = base + ".failed";
        }
    }

    public static final class Configuration {
        static final String        base          = "configuration";

        public static final String availableKeys = base + ".available keys";
        public static final String read          = base + ".read";
        public static final String write         = base + ".write";
        public static final String error         = base + ".error";

        public static final class PlayerConfiguration {
            static final String        base  = Configuration.base + ".player configuration";

            public static final String read  = base + ".read";
            public static final String write = base + ".write";
        }
    }

    public static final class Disable {
        static final String        base         = "disable";

        public static final String succeeded    = base + ".succeeded";
        public static final String failed       = base + ".failed";
        public static final String elapsed_time = base + ".elapsed time";
    }

    public static final class Economy {
        static final String        base             = "economy";

        public static final String giveNegative     = base + ".give negative amount of money";
        public static final String takeNegative     = base + ".take negative amount of money";
        public static final String not_enough_money = base + ".not enough money";
    }

    public static final class Enable {
        static final String        base         = "enable";

        public static final String succeeded    = base + ".succeeded";
        public static final String failed       = base + ".failed";
        public static final String elapsed_time = base + ".elapsed time";
    }

    public static final class EventManager {
        static final String        base         = "event manager";

        public static final String initializing = base + ".initializing";
    }

    public static final class Help {
        static final String        base          = "help";

        public static final String topicNotFound = base + ".topic not found";
    }

    public static final class Message {
        static final String        base                = "message";

        public static final String invalid_language    = base + ".invalid language";
        public static final String no_help_file        = base + ".no help file found";
        public static final String no_config_help_file = base + ".no configuration help file found";
    }

    public static final class Language {
        static final String        base          = "language";

        public static final String concatenation = base + ".concatenation";
        public static final String yes           = base + ".yes";
        public static final String no            = base + ".no";
    }

    public static final class PermissionManager {
        static final String        base         = "permission manager";

        public static final String initializing = base + ".initializing";
    }

    public static final class PluginInfo {
        static final String        base            = "plugin information";

        public static final String description     = base + ".description";
        public static final String authors         = base + ".authors";
        public static final String website         = base + ".website";
        public static final String version         = base + ".version";
        public static final String updateAvailable = base + ".update available";
        public static final String helpLine        = base + ".help line";
    }

    public static final class Reload {
        static final String        base           = "reload";

        public static final String started        = base + ".started";
        public static final String started_server = base + ".started (to server)";
        public static final String succeeded      = base + ".succeeded";
        public static final String failed         = base + ".failed";
    }

    public static final class Save {
        static final String        base           = "save";

        public static final String started        = base + ".started";
        public static final String started_server = base + ".started (to server)";
        public static final String succeeded      = base + ".succeeded";
        public static final String failed         = base + ".failed";
    }

    public static final class SeizeManager {
        static final String        base         = "seize manager";

        public static final String initializing = base + ".initializing";
    }

    public static final class SignManager {
        static final String        base         = "sign manager";

        public static final String initializing = base + ".initializing";
    }

    public static final class TaskManager {
        static final String        base         = "task manager";

        public static final String initializing = base + ".initializing";
    }

    public static final class TradeManager {
        static final String        base         = "trade manager";

        public static final String initializing = base + ".initializing";
    }

    public static final class Updater {
        static final String base = "updater";

        public static final class UpdateCheck {
            static final String        base         = Updater.base + ".update check";

            public static final String initializing = base + ".initializing";
            public static final String started      = base + ".started";
            public static final String disabled     = base + ".disabled";
        }

        public static final class Update {
            static final String        base         = Updater.base + ".update";

            public static final String initializing = base + ".initializing";
            public static final String started      = base + ".started";
            public static final String disabled     = base + ".disabled";
        }
    }

    public static final class WorldManager {
        static final String        base         = "world manager";

        public static final String initializing = base + ".initializing";
    }
}
