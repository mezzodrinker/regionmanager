package mezzo.zz_bukkit.regionmanager.zz_old.message;

import java.io.IOException;
import java.io.InputStream;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;

import mezzo.util.configuration.yaml.YamlConfiguration;
import mezzo.zz_bukkit.regionmanager.zz_old.Component;
import mezzo.zz_bukkit.regionmanager.zz_old.exceptions.message.MissingMessageException;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * <code>MessageManager</code>
 * 
 * @author mezzodrinker
 */
public class MessageManager implements Component {
    private String             currentLanguage  = null;
    private YamlConfiguration  messages         = null;
    private YamlConfiguration  help             = null;
    private YamlConfiguration  configHelp       = null;
    private InputStream        streamMessages   = null;
    private InputStream        streamHelp       = null;
    private InputStream        streamConfigHelp = null;

    public static final String DEFAULT_LANGUAGE = "en-US";

    // Configuration options for the messages configuration
    public static final class ConfigHelp {
        public static final String general = "general";
    }

    public static final String HEADING = "&7-=========- &3RegionManager &7-=========-&r\n";

    public void loadLanguage(String language) throws IOException {
        currentLanguage = language;

        InputStream stream = MessageManager.class.getClassLoader().getResourceAsStream(language + ".yml");
        if (stream == null) throw new IllegalArgumentException("invalid language " + language);
        streamMessages = stream;

        stream = MessageManager.class.getClassLoader().getResourceAsStream("help-" + language + ".yml");
        if (stream == null) throw new IllegalArgumentException("no help file for language " + language + " found");
        streamHelp = stream;

        stream = MessageManager.class.getClassLoader().getResourceAsStream("config-help-" + language + ".yml");
        if (stream == null) throw new IllegalArgumentException("no config help file for language " + language + " found");
        streamConfigHelp = stream;

        load();
    }

    public void loadConfig() throws IOException {
        messages = YamlConfiguration.loadConfiguration(streamMessages);
    }

    public void loadHelp() throws IOException {
        help = YamlConfiguration.loadConfiguration(streamHelp);
    }

    public void loadConfigHelp() throws IOException {
        configHelp = YamlConfiguration.loadConfiguration(streamConfigHelp);
    }

    public YamlConfiguration messages() {
        return messages;
    }

    public YamlConfiguration help() {
        return help;
    }

    public YamlConfiguration configHelp() {
        return configHelp;
    }

    @SafeVarargs
    public final String message(String key, Entry<String, Object>... replacements) throws MissingMessageException {
        String raw = messages().getString(key);
        if (raw == null) throw new MissingMessageException(key, currentLanguage);
        for (Entry<String, Object> entry : replacements) {
            raw = raw.replace(entry.getKey(), String.valueOf(entry.getValue()));
        }
        return raw;
    }

    @SafeVarargs
    public final String messageWrapped(String key, Entry<String, Object>... replacements) {
        try {
            return message(key, replacements);
        } catch (MissingMessageException e) {
            return "(message '" + key + "' not found)";
        }
    }

    @Override
    public String getID() {
        return "messages";
    }

    @Override
    public void load() throws IOException {
        loadConfig();
        loadHelp();
        loadConfigHelp();
    }

    @Override
    public void save() throws IOException {}

    public static final void sendMessage(CommandSender sender, String message) {
        sender.sendMessage(format(message));
    }

    public static final Entry<String, Object> replacement(String value, Object replacement) {
        return new SimpleEntry<String, Object>(value, replacement);
    }

    public static final String format(String message) {
        String ret = message;
        for (ChatColor c : ChatColor.values()) {
            ret = ret.replace("&" + c.getChar(), c.toString());
        }
        return ret;
    }
}
