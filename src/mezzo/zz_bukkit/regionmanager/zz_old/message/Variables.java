package mezzo.zz_bukkit.regionmanager.zz_old.message;

/**
 * <code>Variables</code>
 * 
 * @author mezzodrinker
 */
public final class Variables {
    public static final String time                  = "<time>";
    public static final String authors               = "<authors>";
    public static final String website               = "<website>";
    public static final String yesNo                 = "<yes/no>";
    public static final String command               = "<command>";
    public static final String version               = "<version>";
    public static final String page                  = "<page>";
    public static final String topic                 = "<topic>";
    public static final String option                = "<option>";
    public static final String options               = "<options>";
    public static final String value                 = "<value>";
    public static final String source                = "<source>";
    public static final String component             = "<component>";
    public static final String syntax                = "<syntax>";
    public static final String argumentCount         = "<argument count>";
    public static final String argumentCount_correct = "<correct argument count>";
    public static final String permission            = "<permission>";
    public static final String errorType             = "<error type>";
    public static final String errorMessage          = "<error message>";
    public static final String world                 = "<world>";
    public static final String region                = "<region>";
}
